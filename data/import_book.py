# coding: utf-8

import csv, code, unicodedata, sys, os, json
sys.path.append('./')
from abceedapi.mod_books.models import Test
from abceedapi import db
from abceedapi.mod_books.models import (
    Book, BookPackage, BookResource,
    Question, QuestionGroup,
    Choice, Explanation, Script, Audio,
    Vocabulary, Publisher, Author
)

print "14"


def tsv_read(filename, encoding):
    print "18"
    with open(filename, "rU") as f:
        csvfile = csv.reader(f, delimiter = '\t',dialect=csv.excel_tab)
        rows = [[unicodedata.normalize('NFC', c.decode(encoding)) for c in r] for r in csvfile]
        return rows


def to_img_html(book_id, filename):
    print "26"
    print "book_id: " + str(book_id)
    print "filename: " + str(filename)
    return '<img src="http://d35k757r37s917.cloudfront.net/' + str(book_id) + '/images/' + str(filename) + '"></img>'


def create_script(group, body, exp, book_id):
    print "33"
    script = Script()
    script.script_body = to_img_html(book_id, body)
    script.script_explanation = to_img_html(book_id, exp)
    script.question_group = group
    db.session.add(script)
    return script


def create_choice(question, label, value):
    print "41"
    choice = Choice()
    choice.choice_label = label.rstrip('\r\n')
    choice.choice_value = value
    choice.question = question
    db.session.add(choice)
    return choice


def create_explanation(question=None, body=None, group=None, book_id=None):
    print "53"
    explanation = Explanation()
    explanation.explanation_body = to_img_html(book_id, body)
    print "56"
    if question: explanation.question = question
    if group: explanation.question_group = group
    db.session.add(explanation)
    return explanation


def content(book_id, question, row, test):
    print "66"
    question.question_type = 9
    question.interaction_type = 10
    question_index = row[2]
    part = row[4]
    question_name = row[5]
    # to-do with case set name image-vobucary
    # question_body = 'Q.' + str(row[2]) + '_explanation.png'
    question_body = row[6]

    if question_index : question.question_index = question_index
    if part : question.part = part
    if question_name : question.name = question_name
    if question_body : question.question_body = to_img_html(book_id, question_body)
    print question.to_dict(expand=True)


def part1(book_id, question, row, test):
    print "76"
    test_num = row[1]
    question.question_index = row[2]
    question.question_type = 1
    question.interaction_type = 2
    question.part = row[4]
    question.name = row[5]
    question.section = row[6]
    question.question_num = row[7]
    question.label = row[8]
    question.correct_response = row[9]
    choice_a = row[10]
    choice_b = row[11]
    choice_c = row[12]
    choice_d = row[13]
    explanation = row[14]
    question.question_body = to_img_html(book_id, row[15])
    question.score = 1
    choices = []
    choices.append(create_choice(question=question, label=choice_a, value='A'))
    choices.append(create_choice(question=question, label=choice_b, value='B'))
    choices.append(create_choice(question=question, label=choice_c, value='C'))
    choices.append(create_choice(question=question, label=choice_d, value='D'))
    question.choices = choices
    explanations = []
    if explanation: explanations.append(create_explanation(question=question, body=explanation, book_id=book_id))
    if explanations : question.explanations = explanations
    print question.to_dict(expand=True)


def part2(book_id, question, row, test):
    print "107"
    test_num = row[1]
    question.question_index = row[2]
    question.question_type = 2
    question.interaction_type = 2
    question.part = row[4]
    question.name = row[5]
    question.section = row[6]
    question.question_num = row[7]
    question.label = row[8]
    question.correct_response = row[9]
    question.question_body = row[10]
    choice_a = row[11]
    choice_b = row[12]
    choice_c = row[13]
    explanation = row[14]
    question.score = 1
    choices = []
    choices.append(create_choice(question=question, label=choice_a, value='A'))
    choices.append(create_choice(question=question, label=choice_b, value='B'))
    choices.append(create_choice(question=question, label=choice_c, value='C'))
    question.choices = choices
    explanations = []
    if explanation: explanations.append(create_explanation(question=question, body=explanation, book_id=book_id))
    if explanations : question.explanations = explanations
    print question.to_dict(expand=True)


def part34(book_id, question, row, test):
    print "144"
    test_num = row[1]
    question.question_index = row[2]
    question_type = row[3]
    if question_type == "TOEIC_PART_3":
        question.question_type = 3
    if question_type == "TOEIC_PART_4":
        question.question_type = 4
    question.interaction_type = 2
    question.part = row[4]
    question.name = row[5]
    question.section = row[6]
    question.question_num = row[7]
    question.label = row[8]
    question.correct_response = row[9]
    question.question_body = row[10]
    choice_a = row[11]
    choice_b = row[12]
    choice_c = row[13]
    choice_d = row[14]
    group_name = row[15]

    explanation = row[16]
    group_script = row[17]
    group_script_exp = row[18]
    question.score = 1
    if row[19]:
        print "175"
        script = Script()
        script.script_body = to_img_html(book_id, row[19])
        script.question = question
        db.session.add(script)

    if row[20]:
        print "185"
        script = Script()
        script.script_body = to_img_html(book_id, row[20])
        script.question = question
        db.session.add(script)

    if row[21]:
        print "185"
        script = Script()
        script.script_body = to_img_html(book_id, row[21])
        script.question = question
        db.session.add(script)

    if not group_name: raise Exception('group_name is not defined.')
    group = None
    gruops = QuestionGroup.query.filter_by(test_id=test.id).filter_by(name=group_name)

    if gruops.count() == 1:
        group = gruops.first()
    elif gruops.count() == 0:
        print 'create new question_group object'
        group = QuestionGroup(test)
        group.name = group_name
        db.session.add(group)
    else:
        raise Exception('question_group is not unique.')

    if not group : Exception('question_group is not found.')
    question.question_group = group

    choices = []
    if choice_a: choices.append(create_choice(question=question, label=choice_a, value='A'))
    if choice_b: choices.append(create_choice(question=question, label=choice_b, value='B'))
    if choice_c: choices.append(create_choice(question=question, label=choice_c, value='C'))
    if choice_d: choices.append(create_choice(question=question, label=choice_d, value='D'))
    if choices : question.choices = choices

    explanations = []
    if explanation: explanations.append(create_explanation(question=question, body=explanation, book_id=book_id))
    if explanations : question.explanations = explanations

    group_scripts = []
    if group_script : group_scripts.append(create_script(group=group, body=group_script, exp=group_script_exp, book_id=book_id))
    if group_scripts : group.scripts = group_scripts
    # print group_scripts
    # print question.to_dict(expand=True)
    # print group.to_dict(expand=True)


def part5(book_id, question, row, test):
    print "202"
    test_num = row[1]
    question.question_index = row[2]
    question.question_type = 5
    question.interaction_type = 2
    question.part = row[4]
    question.name = row[5]
    question.section = row[6]
    question.question_num = row[7]
    question.label = row[8]
    question.correct_response = row[9]
    question.question_body = row[10]
    choice_a = row[11]
    choice_b = row[12]
    choice_c = row[13]
    choice_d = row[14]
    explanation = row[15]
    question.score = 1
    choices = []
    if choice_a: choices.append(create_choice(question=question, label=choice_a, value='A'))
    if choice_b: choices.append(create_choice(question=question, label=choice_b, value='B'))
    if choice_c: choices.append(create_choice(question=question, label=choice_c, value='C'))
    if choice_d: choices.append(create_choice(question=question, label=choice_d, value='D'))
    if choices : question.choices = choices
    explanations = []
    if explanation: explanations.append(create_explanation(question=question, body=explanation, book_id=book_id))
    if explanations : question.explanations = explanations
    print question.to_dict(expand=True)



def part6(book_id, question, row, test):
    print "234"
    test_num = row[1]
    question.question_index = row[2]
    question.question_type = 6
    question.interaction_type = 2
    question.part = row[4]
    question.name = row[5]
    question.section = row[6]
    question.question_num = row[7]
    question.label = row[8]
    question.correct_response = row[9]
    choice_a = row[10]
    choice_b = row[11]
    choice_c = row[12]
    choice_d = row[13]
    group_name = row[14]
    explanation = row[15]
    script1 = row[16]
    script1_exp = row[17]
    question.score = 1
    if not group_name: raise Exception('group_name is not defined.')
    group = None
    gruops = QuestionGroup.query.filter_by(test_id=test.id).filter_by(name=group_name)
    if gruops.count() == 1:
        group = gruops.first()
    elif gruops.count() == 0:
        print 'create new question_group object'
        group = QuestionGroup(test)
        group.name = group_name
        db.session.add(group)
    else:
        raise Exception('question_group is not unique.')
    if not group : Exception('question_group is not found.')
    question.question_group = group
    scripts = []
    if script1 : scripts.append(create_script(group=group, body=script1, exp=script1_exp, book_id=book_id))
    if scripts : group.scripts = scripts
    choices = []
    if choice_a: choices.append(create_choice(question=question, label=choice_a, value='A'))
    if choice_b: choices.append(create_choice(question=question, label=choice_b, value='B'))
    if choice_c: choices.append(create_choice(question=question, label=choice_c, value='C'))
    if choice_d: choices.append(create_choice(question=question, label=choice_d, value='D'))
    if choices : question.choices = choices
    explanations = []
    if explanation: explanations.append(create_explanation(question=question, body=explanation, book_id=book_id))
    if explanations : question.explanations = explanations
    print question.to_dict(expand=True)
    print group.to_dict(expand=True)


def part7(book_id, question, row, test):
    print "285"
    test_num = row[1]
    question.question_index = row[2]
    question.question_type = 7
    question.interaction_type = 2
    question.part = row[4]
    question.name = row[5]
    question.section = row[6]
    question.question_num = row[7]
    question.label = row[8]
    question.correct_response = row[9]
    question.question_body = row[10]
    question.score = 1
    choice_a = row[11]
    choice_b = row[12]
    choice_c = row[13]
    choice_d = row[14]
    group_name = row[15]
    explanation = row[16]
    script1 = row[17]
    script1_exp = row[18]
    script2 = None
    script2_exp = None
    script3 = None
    script3_exp = None
    try:
        script2 = row[19]
        script2_exp = row[20]
        script3 = row[21]
        script3_exp = row[22]
    except:
        pass
    if not group_name: raise Exception('group_name is not defined.')
    group = None
    gruops = QuestionGroup.query.filter_by(test_id=test.id).filter_by(name=group_name)
    if gruops.count() == 1:
        group = gruops.first()
    elif gruops.count() == 0:
        print 'create new question_group object'
        group = QuestionGroup(test)
        group.name = group_name
        db.session.add(group)
    else:
        raise Exception('question_group is not unique.')
    if not group : Exception('question_group is not found.')
    question.question_group = group
    scripts = []
    if script1 : scripts.append(create_script(group=group, body=script1, exp=script1_exp, book_id=book_id))
    if script2 : scripts.append(create_script(group=group, body=script2, exp=script2_exp, book_id=book_id))
    if script3 : scripts.append(create_script(group=group, body=script3, exp=script3_exp, book_id=book_id))
    if scripts : group.scripts = scripts
    choices = []
    if choice_a: choices.append(create_choice(question=question, label=choice_a, value='A'))
    if choice_b: choices.append(create_choice(question=question, label=choice_b, value='B'))
    if choice_c: choices.append(create_choice(question=question, label=choice_c, value='C'))
    if choice_d: choices.append(create_choice(question=question, label=choice_d, value='D'))
    if choices : question.choices = choices
    explanations = []
    if explanation: explanations.append(create_explanation(question=question, body=explanation, book_id=book_id))
    if explanations : question.explanations = explanations
    print question.to_dict(expand=True)
    print group.to_dict(expand=True)




def vocab(book_id, question, row, test):
    print "355"
    if row[7]:
        question.question_index = row[2]
        question.question_type = 8
        question.interaction_type = 1
        question.part = row[4]
        question.name = row[5]
        question.section = row[6]
        question.question_num = row[7]
        question.label = row[8]
        question.score = 1
        question.correct_response = row[9]
        question.question_body = to_img_html(book_id,row[14])
        text_en = row[10]
        text_ja = row[11]
        sentence_en = row[12]
        sentence_ja = row[13]
        explanation = row[15]
        if text_en : question.correct_response = text_en
        vocab = None
        print question.id
        if len(question.vocabularies) == 1:
            vocab = question.vocabularies[0]
        elif len(question.vocabularies)  == 0:
            print 'create new vocab object'
            vocab = Vocabulary()
            vocab.question = question
            db.session.add(vocab)
        else:
            raise Exception('vocab is not unique.')
        vocab.text_en = text_en
        vocab.text_ja = text_ja
        vocab.vocab_type = row[9]
        vocab.sentence_en = sentence_en
        vocab.sentence_ja = sentence_ja
        db.session.commit()

        explanations = []
        if explanation: explanations.append(create_explanation(question=question, body=explanation, book_id=book_id))
        if explanations : question.explanations = explanations
        print question.to_dict(expand=True)
    else:
        if row[10]:
            text_en = row[10]
        else:
            return
        text_en_find =  "'" + row[10] +"'"
        sqlGetVocab  = 'select question_id from vocabulary where text_en = '+ text_en_find + ' order by date_created desc limit 1'
        resultVocab  = db.engine.execute(sqlGetVocab)

        vocab_type = row[9]
        text_ja    = row[11]

        for row in resultVocab:
            if row[0]:
                print "print new mean for vocab"
                oldQuestion = Question.query.filter_by(id = row[0]).first()
                vocab = Vocabulary()
                vocab.question   = oldQuestion
                vocab.vocab_type = vocab_type
                vocab.text_en    = text_en
                vocab.text_ja    = text_ja

encoding = 'utf8'
update_question = {
    'TOEIC_PART_1': part1,
    'TOEIC_PART_2': part2,
    'TOEIC_PART_3': part34,
    'TOEIC_PART_4': part34,
    'TOEIC_PART_5': part5,
    'TOEIC_PART_6': part6,
    'TOEIC_PART_7': part7,
    'VOCABULARY_TEST': vocab,
    'CONTENT': content
}


if __name__ == '__main__':

    database = os.environ.get('SQLALCHEMY_DATABASE_URI')
    print database
    if not database:
        print 'The environment variable "SQLALCHEMY_DATABASE_URI" is not set'
    else:
        print 'SQLALCHEMY_DATABASE_URI = ' + database

    # 書籍を取得する
    book_id = input_var = raw_input("\nEnter book_id: ")
    book = Book.query.filter_by(id=book_id).first()
    # print json.dumps(book.to_dict(), indent=4, ensure_ascii=False)
    print "445"
    # テストを取得する
    if not book.tests:
        print "448"
        test = Test(name='TEST 1', book=book)
        db.session.add(test)
        db.session.commit()

    print '\n'
    for idx, test in enumerate(book.tests):
        print idx, 'test name = ' + test.name

    test_index = int(raw_input("\nEnter test number [0-9]: "))
    test = book.tests[test_index]
    # print json.dumps(test.to_dict(), indent=4, ensure_ascii=False)

    # tsvを読み込む
    tsv_filename = raw_input("\nEnter tsv filepath: ")
    tsv_rows = tsv_read(tsv_filename, encoding)
    data = []
    print "465"
    for row in tsv_rows:
        data.append(row)

    # update data
    for row in data:
        print "471"
        # get csv cell's values
        try:
            # print row[1]
            test_num = row[1]
            question_index = row[2]
            question_type = row[3]
        except:
            continue
        print "481"
        # validation
        try:
            print "484"
            if not question_type: raise
            print question_index
            question_index = int(question_index)
        except:
            print "489"
            continue
        #print "465"
        # update data
        questions = Question.query.filter_by(test_id=test.id).filter_by(question_index=question_index)
        print questions.count()
        if questions.count() == 1:
            question = questions.first()
            print "testttt"
            try:
                update_question[question_type](book_id, question, row, test)
            except:
                raise
        elif questions.count() == 0:
            print 'create new question object'
            question = Question(test, question_type, "", 2, "")
            db.session.add(question)
            try:
                update_question[question_type](book_id, question, row, test)
            except:
                raise
        else:
            pass

    db.session.commit()

    # Comment out when starting interactive shell
    #code.InteractiveConsole(globals()).interact()

    s3_key = book_id + '/' + book_id + '.zip'
    pack = BookPackage(book=book, s3_bucket='cdn.abceed2.com', s3_key=s3_key)
    pack.s3_url = 'https://d35k757r37s917.cloudfront.net/' + s3_key
    db.session.add(pack)
    db.session.commit()

    print 'DONE'

    print '\nfinish'
