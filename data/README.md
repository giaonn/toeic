# Import Book & Audio Script
# Read

## create book_format.xlsx and audio_format.xlsx

Register the book information according to the format.
For details, please check the sample data described in the format.
You can also see what each line should write in by checking import_book.py.

## convert xlsx to tsv

Change the sheet of xlsx to tsv.
Copy and paste into a text editor


## upload files

Register files (mp3/png) from the web administration screen.
If there is no test, register a test.

http://d1i3iegge9runl.cloudfront.net

Please check the book ID to use it for the following step.


## run import script

Start the local dev environment, change dir to project.

> Please check the README of the project for how to create the local　dev environment

```
$ cd path/to/project
$ ls -la
total 3126
drwxrwxrwx.  1 root    root       4096 Feb 25 03:07 .
drwx------. 12 vagrant vagrant    4096 Feb 16 04:24 ..
drwxrwxrwx.  1 root    root       4096 Feb 23 10:39 abceedapi
drwxrwxrwx.  1 root    root       8192 Feb 25 03:04 data
-rwxrwxrwx.  1 root    root          0 Feb 25 03:07 db.sqlite
drwxrwxrwx.  1 root    root       4096 Feb 25 01:34 docs
-rwxrwxrwx.  1 root    root         38 Feb 25 01:33 .gitignore
-rwxrwxrwx.  1 root    root        713 Oct 27 20:06 init_db.py
-rwxrwxrwx.  1 root    root    2981544 Oct 27 14:57 libmysqlclient.so.18
drwxrwxrwx.  1 root    root       4096 Oct 27 15:06 MySQLdb
-rwxrwxrwx.  1 root    root       2352 Oct 27 14:57 _mysql_exceptions.py
-rwxrwxrwx.  1 root    root       3835 Oct 27 14:58 _mysql_exceptions.pyc
-rwxrwxrwx.  1 root    root     172090 Oct 27 14:57 _mysql.so
-rwxrwxrwx.  1 root    root       4658 Feb 25 01:47 README.md
-rwxrwxrwx.  1 root    root        620 Feb  8 09:12 requirements.txt
-rwxrwxrwx.  1 root    root        206 Feb 23 02:48 run.py
-rwxrwxrwx.  1 root    root       1766 Feb 23 12:53 zappa_settings.json
-rwxrwxrwx.  1 root    root        602 Nov  9 12:49 zappa_with_iamauth.patch
```

export database environment.

```
$ export SQLALCHEMY_DATABASE_URI='mysql://globee:Globee612@abceed-db.c0pvfeqpfvfq.ap-northeast-1.rds.amazonaws.com:3306/abceed_db'
```

run import_book.py

```
$ python data/import_book.py
SQLALCHEMY_DATABASE_URI = mysql://globee:Globee612@abceed-db.c0pvfeqpfvfq.ap-northeast-1.rds.amazonaws.com:3306/abceed_db
Enter book_id: e81dd455-d783-41bf-acc0-e13ee21644c1

{ ... show json data ...}

0 test name = TEST 1
Enter test number [0-9]: 0

{ ... show json data ...}

Enter tsv filepath: data/book.tsv

{ ... show json data ...}


Upload all the audio and image resources to s3.
and Upload the resource with zip as follows


s3_bucket : cdn.abceed2.com
s3_key : e7c6db16-ac24-4df3-ba3a-3cbbdcfd74eb/e7c6db16-ac24-4df3-ba3a-3cbbdcfd74eb.zip
s3_url : https://d35k757r37s917.cloudfront.net/e7c6db16-ac24-4df3-ba3a-3cbbdcfd74eb/e7c6db16-ac24-4df3-ba3a-3cbbdcfd74eb.zip

finish
```

Please update zip file manually.

run import_audio.py

```
$ python data/import_book.py
SQLALCHEMY_DATABASE_URI = mysql://globee:Globee612@abceed-db.c0pvfeqpfvfq.ap-northeast-1.rds.amazonaws.com:3306/abceed_db
Enter book_id: e81dd455-d783-41bf-acc0-e13ee21644c1

{ ... show json data ...}

0 test name = TEST 1
Enter test number [0-9]: 0

{ ... show json data ...}

Enter tsv filepath: data/audio.tsv

{ ... show json data ...}

finish
```

## clear cache

Clear redis & aws cloud front cache.

> From the web administration screen you can clear the redis cache by sending an API request to /flushall on the dev[開発] tab.
> Please invalidate CloudFront from the AWS console.

## Mapping with abceed analytics data

1. Connect to DB with mysql database connection tool
 * host: abceed-db.c0pvfeqpfvfq.ap-northeast-1.rds.amazonaws.com
 * user: globee
 * pass: Globee612
 * database: abceed_db
2. Open test table
3. Select test from test table
4. Insert analytics test id (e.g. asahi_book_1_test_1) in analytics_test_id field
5. save

# Update specific data.

```
$ python

# import models
>> from abceedapi import db
>> from abceedapi.mod_books.models import Book, Test, Question

# get instance
>> test = Test.query.filter_by(book_id='e81dd455-d783-41bf-acc0-e13ee21644c1').first()
>> question = Question.query.filter_by(test_id=test.id).filter_by(question_index=3).first()

# update data
>> question.name = 'new name'

# save
>> db.session.commi()

```

For details, refer to the following link.
http://www.sqlalchemy.org/

