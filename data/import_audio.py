# coding: utf-8

import csv, code, unicodedata, sys, os, json
sys.path.append('./')

from abceedapi import db
from abceedapi.mod_books.models import (
    Book, BookPackage, BookResource,
    Question, QuestionGroup,
    Choice, Explanation, Script, Audio,
    Vocabulary, Publisher, Author
)


def tsv_read(filename, encoding):
    print "16"
    with open(filename, "rU") as f:
        csvfile = csv.reader(f, delimiter = '\t',dialect=csv.excel_tab)
        rows = [[unicodedata.normalize('NFC', c.decode(encoding)) for c in r] for r in csvfile]
        return rows

def create_audio(question=None, group=None, resource=None, sound_start_position=None, sound_end_position=None):
    print "23"
    audio = Audio()
    audio.resource = resource
    if question: audio.question = question
    if group: audio.question_group = group
    if sound_start_position: audio.sound_start_position = sound_start_position
    if sound_end_position: audio.sound_end_position = sound_end_position
    db.session.add(audio)
    return audio

encoding = 'utf8'


if __name__ == '__main__':
    print "37"
    database = os.environ.get('SQLALCHEMY_DATABASE_URI')
    if not database:
        print 'The environment variable "SQLALCHEMY_DATABASE_URI" is not set'
    else:
        print 'SQLALCHEMY_DATABASE_URI = ' + database

    # 書籍を取得する
    book_id = input_var = raw_input("\nEnter book_id: ")
    book = Book.query.filter_by(id=book_id).first()
    print json.dumps(book.to_dict(), indent=4, ensure_ascii=False)
    print "48"
    # テストを取得する
    if not book.tests:
        print "51"
        test = Test(name='TEST 1', book=book)
        db.session.add(test)
        db.session.commit()
    print "54"
    print '\n'
    for idx, test in enumerate(book.tests):
        print idx, 'test name = ' + test.name

    test_index = int(raw_input("\nEnter test number [0-9]: "))
    test = book.tests[test_index]
    # print json.dumps(test.to_dict(), indent=4, ensure_ascii=False)

    # tsvを読み込む
    tsv_filename = raw_input("\nEnter tsv filepath: ")

    # code.InteractiveConsole(globals()).interact()

    tsv_rows = tsv_read(tsv_filename, encoding)
    data = []
    for row in tsv_rows:
        data.append(row)

    resources = BookResource.query.filter_by(book_id=book.id).filter_by(resource_type=1)
    print "74"
    for row in data:
        try:
            print row[1]
            question_index = row[1]
            question_index = int(question_index)
            start_at = row[2]
            stop_at = row[3]
            filename = row[4]            
        except:
            continue
        print "85"
        questions = Question.query.filter_by(test_id=test.id).filter_by(question_index=question_index)
        audios = BookResource.query.filter_by(book_id=book.id).filter_by(filename=filename)
        audio = None
        if questions.count() == 1 and audios.count() == 1:
            question = questions.first()
            resource = audios.first()
            if question.question_group:
                question.question_group.audios = []
                audio = create_audio(group=question.question_group, resource=resource, sound_start_position=start_at, sound_end_position=stop_at)
            else:
                question.audios = []
                audio = create_audio(question=question, resource=resource, sound_start_position=start_at, sound_end_position=stop_at)
            print audio.to_dict()
        elif audios.count() == 0:
            print 'question or audio is not found.'
            continue
        else:
            pass
            # raise Exception('filename or question_index is not unique.')

    db.session.commit()

    print '\nfinish'
