from abceedapi import db
db.create_all()

# if drop all tables
# USE abceeed_db;
# SET FOREIGN_KEY_CHECKS=0;
# DROP TABLES advertisement;
# DROP TABLES user_activity_item;
# DROP TABLES user_purchase_item;
# DROP TABLES user_recommendation_item;
# DROP TABLES user;
# DROP TABLES explanation;
# DROP TABLES explanation_audio;
# DROP TABLES question_audio;
# DROP TABLES question_choice;
# DROP TABLES question_group;
# DROP TABLES question_script;
# DROP TABLES question;
# DROP TABLES test_package;
# DROP TABLES test_resource;
# DROP TABLES test;
# DROP TABLES book_author;
# DROP TABLES book_categories;
# DROP TABLES book_category;
# DROP TABLES book_publisher;
# DROP TABLES book;
# SET FOREIGN_KEY_CHECKS=1;