from abceedapi import db
from abceedapi.mod_books.models import (
    Book
)
from flask import jsonify
from abceedapi.mod_users.models import User, PurchaseItem


def import_book(event, context):
    user = User.query.filter_by(
        cognito_pool_id='ap-northeast-1:ccb05128-611f-4ca2-b5d9-d20fcf27c3dd',
        cognito_user_id='ap-northeast-1:dc661f89-f3c1-4a43-ab18-beaca9e025c8').first()
    result = []
    items = db.session.query(PurchaseItem).filter_by(user_id=user.id).order_by(-PurchaseItem.date_created)

    print items
    # user.purchase_items
    for item in items:
        result.append(item.book.to_dict())

    # books = Book.query.filter_by(is_free=1).filter_by(is_public=1)
    # for book in books:
    #     result.append(book.to_dict())

    return jsonify(result), 200

if __name__ == "__main__":
    print import_book({},{})