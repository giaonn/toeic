#!/bin/bash

mkdir -p /home/vagrant/.ssh
wget --no-check-certificate https://raw.github.com/mitchellh/vagrant/master/keys/vagrant.pub -O /home/vagrant/.ssh/authorized_keys
chmod 0700 /home/vagrant/.ssh
chmod 0600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant /home/vagrant/.ssh


#install packages
yum install -y gcc libffi-devel python-devel openssl-devel zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel git
curl -L https://bootstrap.pypa.io/get-pip.py | sudo python


#install pyenv and virtualenv
curl -L https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash
cat << 'EOF' >> ~/.bash_profile
### virtualenv
export PATH="/home/vagrant/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
EOF
source ~/.bash_profile


# install virtualenvwrapper
pip install virtualenvwrapper
export WORKON_HOME="~/python-venv"
mkdir -p $WORKON_HOME
virtualenvwrapper_sh=$(whereis virtualenvwrapper.sh | cut -d : -f2)
echo """
if [ -f ${virtualenvwrapper_sh} ]; then
    export WORKON_HOME=$HOME/.virtualenvs
    source ${virtualenvwrapper_sh}
fi
""" >> ~/.bash_profile
source ${virtualenvwrapper_sh}


# install python
pyenv install 2.7.12
pyenv local 2.7.12
pyenv versions

# install awscli
pip install awscli


# install mysql 
yum -y install mysql-server

# install er
yum -y install graphviz-devel
pip install eralchemy
#eralchemy -i sqlite:///`pwd`/db.sqlite -o ER-database.png