# coding: utf-8

import uuid
from flask import Blueprint, abort, jsonify, request
from flask_sqlalchemy import sqlalchemy
from abceedapi import db, logger, connection
from abceedapi.mod_books.models import Test
from abceedapi.utils.auth_utils import (cognito_auth_required,cognito_auth_option)
from abceedapi.mod_books.models import (Question,Book)
from sqlalchemy import func, create_engine
from .models import User, PurchaseItem, LearningActivityItem
from datetime import datetime
import dateutil.parser as parser

from pprint import pprint

mod_users = Blueprint('users', __name__, url_prefix='/users')

#-------------Handle old------------------
@mod_users.route('/me', methods=['GET'])
def get_or_create_user():
    remote_user = request.remote_user
    if not remote_user or type(remote_user) is not dict:
        abort(401, {'message': 'unauthorized.'})

    try:
        cognito_pool_id = remote_user.get('cognitoIdentityPoolId')
        cognito_user_id = remote_user.get('cognitoIdentityId')
    except:
        abort(401, {'message': 'unauthorized.'})

    if not cognito_pool_id or not cognito_user_id:
        abort(401, {'message': 'unauthorized.'})

    user = User.query.filter_by(cognito_pool_id=cognito_pool_id, cognito_user_id=cognito_user_id).first()

    if user:
        return jsonify(user.to_dict()), 200
    else:
        newUser = User(
            cognito_pool_id=cognito_pool_id,
            cognito_user_id=cognito_user_id)
        db.session.add(newUser)
        db.session.commit()
        return jsonify(newUser.to_dict()), 201

    abort(404, {'message': 'user cannot be found.'})

@mod_users.route('/me', methods=['PUT'])
@cognito_auth_required()
def update_or_create_user(user=None):
    if request.headers['Content-Type'] != 'application/json':
        abort(400, {'message': 'request must hame "Content-Type: applicatin/json" header.'})

    req_json = request.json if request.json else {}

    user.name = req_json.get('name', user.name)
    user.email = req_json.get('email', user.email)
    user.avatar_image_url = req_json.get('avatar_image_url', user.avatar_image_url)
    user.facebook_token = req_json.get('facebook_token', user.facebook_token)
    user.twitter_token = req_json.get('twitter_token', user.twitter_token)
    user.twitter_token_secret = req_json.get('twitter_token_secret', user.twitter_token_secret)
    user.now_toeic_socre = req_json.get('now_toeic_socre', user.now_toeic_socre)
    user.target_toeic_score = req_json.get('target_toeic_score', user.target_toeic_score)
    user.next_toeic_date = req_json.get('next_toeic_date', user.next_toeic_date)
    db.session.commit()

    return jsonify(user.to_dict()), 200

#-------------Handle new----------------------
@mod_users.route('/handleUser', methods=['GET'])
def registerOrGetUser():

    remote_user = request.remote_user
    if not remote_user or type(remote_user) is not dict:
        abort(401, {'message': 'unauthorized remote user'})
    try:
        cognito_pool_id = remote_user.get('cognitoIdentityPoolId')
        cognito_user_id = remote_user.get('cognitoIdentityId')
    except:
        abort(401, {'message': 'unauthorized.'})

    if not cognito_pool_id or not cognito_user_id:
        abort(401, {'message': 'unauthorized cognito'})

    user = User.query.filter_by(cognito_pool_id = cognito_pool_id, cognito_user_id = cognito_user_id)

    if user:
        if not userExistsOnV1(user.cognito_pool_id, user.cognito_user_id):
            createUserOnV1(user.id, cognito_pool_id, cognito_user_id)
        return  jsonify(user.to_dict()),200
    else:
        newUser =  User(cognito_pool_id = cognito_pool_id, cognito_user_id = cognito_user_id)
        db.session.add(newUser)
        db.session.commit()
        userJustCreated =  User.query.filter_by(cognito_pool_id = cognito_pool_id, cognito_user_id = cognito_user_id)

        if userJustCreated:
            createUserOnV1(user.id, cognito_pool_id, cognito_user_id)

        return jsonify(newUser.to_dict()), 201


@mod_users.route('/handleUser', methods=['PUT'])
@cognito_auth_required()
def UpdateOrGetUser(user=None):
    if not userExistsOnV1(user.cognito_pool_id, user.cognito_user_id):
        abort(400, {'message':'User on V1 null'})
    if request.headers['Content-Type'] != 'application/json':
        abort(400, {'message': 'request must hame "Content-Type: applicatin/json" header.'})

    req_json = request.json if request.json else {}
    try:
        updateUserOnV1(user, req_json)
    except:
        abort(400,{'message' : 'Update user on V1 fail'})

    user.name = req_json.get('name', user.name)
    user.email = req_json.get('email', user.email)
    user.avatar_image_url = req_json.get('avatar_image_url', user.avatar_image_url)
    user.facebook_token = req_json.get('facebook_token', user.facebook_token)
    user.twitter_token = req_json.get('twitter_token', user.twitter_token)
    user.twitter_token_secret = req_json.get('twitter_token_secret', user.twitter_token_secret)
    user.now_toeic_socre = req_json.get('now_toeic_socre', user.now_toeic_socre)
    user.target_toeic_score = req_json.get('target_toeic_score', user.target_toeic_score)
    user.next_toeic_date = req_json.get('next_toeic_date', user.next_toeic_date)
    db.session.commit()

    return jsonify(user.to_dict()), 200


def createUserOnV1(id_user, cognito_pool_id, cognito_user_id):
    params = (id_user, datetime.now().strftime('%Y-%m-%d %H:%M:%S'), cognito_pool_id, cognito_user_id)
    sql = "insert into user(id, cognito_pool_id, cognito_user_id, date_created) values(%s,%s,%s,%s)"
    connection.execute(sql, params)
    connection.close()

def updateUserOnV1(user, req_json):
    params = (
        req_json.get('name', user.name),
        req_json.get('email', user.email),
        req_json.get('avatar_image_url', user.avatar_image_url),
        req_json.get('facebook_token', user.facebook_token),
        req_json.get('twitter_token', user.twitter_token),
        req_json.get('twitter_token_secret', user.twitter_token_secret),
        req_json.get('now_toeic_socre', user.now_toeic_socre),
        req_json.get('target_toeic_score', user.target_toeic_score),
        req_json.get('next_toeic_date', user.next_toeic_date),
        user.cognito_pool_id,
        user.cognito_user_id
    )

    sql = "Update user set name = %s, email = %s , avatar_image_url = %s, " \
          "facebook_token = %s, twitter_token = %s, twitter_token_secret = %s, " \
          "now_toeic_socre = %s, target_toeic_score = %s, next_toeic_date = %s where cognito_pool_id = %s and cognito_user_id = %s"

    connection.execute(sql, params)

def userExistsOnV1(cognito_pool_id, cognito_user_id):

    params = (cognito_pool_id, cognito_user_id)
    sql = "select * from user where cognito_pool_id = %s and cognito_user_id = %s"
    result = connection.execute(sql, params)
    if result.rowcount > 0:
        return True
    else:
        return False

@mod_users.route('/testRegisterUser', methods=['GET'])
def testRegisterUser():
    userId = 'a00f8f74-4b7f-4fe2-9269-e023cf72b033'
    if not userExistsOnV1(userId):
        print 'n'
    else:
        print 'y'
    return 'test'

@mod_users.route('/my_books', methods=['GET'])
@cognito_auth_option()

def get_user_mybooks(user=None):
    # user = User.query.filter_by(
    #     cognito_pool_id='ap-northeast-1:ccb05128-611f-4ca2-b5d9-d20fcf27c3dd',
    #     cognito_user_id='ap-northeast-1:dc661f89-f3c1-4a43-ab18-beaca9e025c8').first()
    #     cognito_user_id='ap-northeast-1:dc661f89-f3c1-4a43-ab18-beaca9e025c8').first()

    result = []
    items  = None
    if user is not None:
        items  =   db.session.query(PurchaseItem).filter_by(user_id=user.id).order_by(-PurchaseItem.date_created)

    # user.purchase_items
    if items is not None:
        for item in items:
            result.append(item.book.to_dict())

    books = Book.query.filter_by(is_free = 1).filter_by(is_public = 1)
    for book in books:
        result.append(book.to_dict())

    return jsonify(result), 200

@mod_users.route('/my_activity', methods=['GET'])
@cognito_auth_required()
def get_user_activity(user=None):
    items = db.session.query(LearningActivityItem).options(
        sqlalchemy.orm.subqueryload('question')
    ).filter_by(user_id=user.id).order_by(-LearningActivityItem.timestamp)

    test_id = request.args.get('test')
    if test_id:
        items = items.filter(LearningActivityItem.question.has(test_id=test_id))

    # start_date = request.args.get('startdate')
    # if start_date:
    #     items = items.filter(LearningActivityItem.timestamp >= start_date)

    # end_date = request.args.get('enddate')
    # if end_date:
    #     items = items.filter(LearningActivityItem.timestamp <= end_date)

    result = []
    for item in items[:300]:
        data_dict = item.to_dict(exclude=['question_id', 'user_id'])
        data_dict['question_id'] = item.question_id
        data_dict['user_id'] = item.user_id
        result.append(data_dict)

    return jsonify(result), 200


@mod_users.route('/my_activity', methods=['POST'])
@cognito_auth_required()
def create_user_activity(user=None):
    if request.headers['Content-Type'] != 'application/json':
        abort(400, {'message': 'request must hame "Content-Type: applicatin/json" header.'})

    bulk = request.args.get('bulk')
    req_json = request.json if request.json else {}

    if bulk and type(req_json) is list:
        result = []
        for row in req_json:
            question_id = row.get('question_id')
            question = Question.query.filter_by(id=question_id).first()

            if not question:
                abort(404, {'message': 'question cannot be found.'})

            item = LearningActivityItem(
                user=user,
                question=question,
                timestamp=row.get('timestamp'),
                learning_status=row.get('learning_status'),
                activity_type=row.get('activity_type')
            )
            item.load_json(row)
            db.session.add(item)
            result.append(item.to_dict())
        db.session.commit()
        return jsonify(result), 200
    else:
        question_id = req_json.get('question_id')
        question = Question.query.filter_by(id=question_id).first()

        if not question:
            abort(404, {'message': 'question cannot be found.'})

        item = LearningActivityItem(
            user=user,
            question=question,
            timestamp=req_json.get('timestamp'),
            learning_status=req_json.get('learning_status'),
            activity_type=req_json.get('activity_type')
        )
        item.load_json(req_json)
        db.session.add(item)
        db.session.commit()
        return jsonify(item.to_dict()), 200


@mod_users.route('/my_learning_status', methods=['GET'])
@cognito_auth_required()
def get_user_learning_status(user=None):

    test_id = request.args.get('test_id')
    if not test_id:
        abort(400, {'message': 'test_id param is required.'})

    test = Test.query.filter_by(id=test_id).first()
    if not test:
        abort(404, {'message': 'test cannot be found.'})

    user_id = "'" + user.id + "'"

    sqlMyAct = 'SELECT ac.*, question.question_type, count(ac.id) as all_activity, sum(case when ac.learning_status in ' \
               ' (3, 4, 5, 6, 7) then 1 else 0 end) as all_activity_right  from (select * from user_activity_item order by `timestamp` desc) as ac' \
               ' join question on question.id = ac.question_id where  user_id =' \
               + str(user_id) + 'and question.test_id = \'' \
               + str(test_id) + '\' group by ac.question_id order by ac.timestamp desc '

    # sqlMyActRight = 'SELECT ac.*,question.question_type,count(*) as all_activity_right from user_activity_item  as ac' \
    #            ' join question on question.id = ac.question_id where  user_id =' \
    #            + str(user_id) + 'and question.test_id = \'' \
    #            + str(test_id) + '\' and (learning_status =3 or learning_status =4 or learning_status =5 or ' \
    #             'learning_status =6 or learning_status =7)  group by ac.question_id order by timestamp desc '
    #
    # sqlSum = 'select my_act.*, coalesce(myactright.all_activity_right, 0) as all_activity_right from ('\
    #         + sqlMyAct + ') as my_act left join (' + sqlMyActRight + ') as myactright on my_act.question_id = myactright.question_id'

    # print sqlMyAct
    result = db.engine.execute(sqlMyAct)
    lst = []

    for row in result:
        jsonTest = {}
        jsonTest['id']                 = row[0]
        jsonTest['date_created']       = convertRFCDate(row[1])
        jsonTest['date_modified']      = convertRFCDate(row[2])
        jsonTest['activity_type']      = row[3]
        jsonTest['timestamp']          = convertRFCDate(row[4])
        jsonTest['learning_status']    = row[5]
        jsonTest['concern_flag']       = row[6]
        jsonTest['user_choice']        = row[7]
        jsonTest['duration']           = row[8]
        jsonTest['user_id']            = row[9]
        jsonTest['question_id']        = row[10]
        jsonTest['question_type']      = row[12]
        jsonTest['all_activity']       = int(row[13])
        jsonTest['all_activity_right'] = int(row[14])

        lst.append(jsonTest)

    return jsonify(lst)

def convertRFCDate(date):
    dateCon = date.strftime("%Y-%m-%d %H:%M:%S")
    dateFar = (parser.parse(dateCon))

    return dateFar.isoformat("T") + "Z"

