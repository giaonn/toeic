# coding: utf-8

import enum
from abceedapi import db
from abceedapi.models import Base
from abceedapi.utils.json_utils import model2dict


class User(Base):
    __tablename__ = 'user'

    # Cognito Identity
    cognito_pool_id = db.Column(db.String(255), nullable=False)
    cognito_user_id = db.Column(db.String(255), nullable=False, unique=True)

    # User Basic Profiles
    name = db.Column(db.String(255), nullable=True)
    email = db.Column(db.String(255), nullable=True)
    avatar_image_url = db.Column(db.Text, nullable=True)

    # SNS Token
    facebook_token = db.Column(db.Text, nullable=True)
    twitter_token = db.Column(db.Text, nullable=True)
    twitter_token_secret = db.Column(db.Text, nullable=True)

    # User Learning Profiles
    now_toeic_socre = db.Column(db.Integer, nullable=True)
    target_toeic_score = db.Column(db.Integer, nullable=True)
    next_toeic_date = db.Column(db.DateTime, nullable=True)

    level = db.Column(db.Integer, nullable=False)
    is_admin = db.Column(db.Boolean, default=False, nullable=False)

    def __init__(self, cognito_pool_id, cognito_user_id):
        self.cognito_pool_id = cognito_pool_id
        self.cognito_user_id = cognito_user_id
        super(User, self).__init__()

    def __repr__(self):
        return '<User %r>' % (self.id)


class ActivityType(enum.Enum):
    INITIALIZED = 1
    ANSWERED = 2
    MANUALLY_CHANGED = 3
    AUTOMATICALLY_CHANGED = 4


class LearningStatus(enum.Enum):
    NOT_LEARNED = 1
    IN_CORRECT = 2
    LEARNED_1 = 3
    NEED_REVIEW_1 = 4
    LEARNED_2 = 5
    NEED_REVIEW_2 = 6
    LEARNED_3 = 7


class LearningActivityItem(Base):
    __tablename__ = 'user_activity_item'

    activity_type = db.Column(db.Integer, nullable=False)
    timestamp = db.Column(db.DateTime, nullable=False)
    learning_status = db.Column(db.Integer, nullable=False)

    # if activity_type == answered
    concern_flag = db.Column(db.Boolean, default=False, nullable=False)
    user_choice = db.Column(db.Text, nullable=True)
    duration = db.Column(db.Float, nullable=True)

    user_id = db.Column(db.String(255), db.ForeignKey('user.id'))
    user = db.relationship(
        'User', backref=db.backref('activity_items', lazy='dynamic'))

    question_id = db.Column(db.String(255), db.ForeignKey('question.id'))
    question = db.relationship(
        'Question', backref=db.backref('activity_items', lazy='dynamic'))

    extended_data = db.Column(db.Text, nullable=True)

    __mapper_args__ = {
        "order_by": 'timestamp'
    }


    def __repr__(self):
        return '<LearningActivityItem %r>' % (self.id)

    def __init__(self, activity_type, timestamp, learning_status, user, question):
        self.activity_type = activity_type
        self.timestamp = timestamp
        self.learning_status = learning_status
        self.user = user
        self.question = question
        super(LearningActivityItem, self).__init__()


class RecommendationItem(Base):
    __tablename__ = 'user_recommendation_item'

    user_id = db.Column(db.String(255), db.ForeignKey('user.id'))
    user = db.relationship('User', uselist=False, backref=db.backref(
        'recommendation_items', lazy='dynamic'))

    book_id = db.Column(db.String(255), db.ForeignKey('book.id'))
    book = db.relationship('Book', uselist=False)


class PurchaseItemType(enum.Enum):
    GOOGLE_PLAY_STORE = 1
    APPLE_APP_STORE = 2


class PurchaseItem(Base):
    __tablename__ = 'user_purchase_item'

    receipt_item_type = db.Column(db.Integer, nullable=False)
    purchase_data = db.Column(db.Text, nullable=False)
    purchase_signature = db.Column(db.Text, nullable=False)

    # date_created = db.Column(db.DateTime, nullable=False)

    user_id = db.Column(db.String(255), db.ForeignKey('user.id'))
    user = db.relationship('User', uselist=False, backref=db.backref(
        'purchase_items', lazy='dynamic'))

    book_id = db.Column(db.String(255), db.ForeignKey('book.id'))
    book = db.relationship('Book', uselist=False, backref=db.backref(
        'purchase_items', lazy='dynamic'))

    def __init__(self, receipt_item_type, book, user):
        self.receipt_item_type = receipt_item_type
        self.book = book
        self.user = user
        super(PurchaseItem, self).__init__()
