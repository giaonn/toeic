import os
BASE_DIR = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))


# SQLALCHEMY_DATABASE_URI = os.environ.get(
# 	'SQLALCHEMY_DATABASE_URI',
# 	'sqlite:///' + os.path.join(BASE_DIR, 'db.sqlite'))
SQLALCHEMY_DATABASE_URI = "mysql://globee:Globee612@abceed-db.c0pvfeqpfvfq.ap-northeast-1.rds.amazonaws.com:3306/abceed_db_0614_dev"
SQLALCHEMY_TRACK_MODIFICATIONS = True
DATABASE_CONNECT_OPTIONS = {}

# SQLALCHEMY_ECHO=True
JSON_AS_ASCII = False

AES_SECRET = 'yRa0SPHMf5xbcS0gfEwrxekORnGNb29aXU5e5605i1y9xd25Ja'

REDIS_URL = os.environ.get(
	'REDIS_URL',
	"redis://localhost:6379/0")

TEST_USER_ID = 'efa721e2-2483-4502-a2c6-d4329e103b2c'
DEBUG = True if os.environ.get('ENV') == 'development' else False

ABCEED_ANALYTICS_API_ENDPOINT = 'https://skqspbonmi.execute-api.ap-northeast-1.amazonaws.com/dev'
