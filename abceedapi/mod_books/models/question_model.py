# coding: utf-8

import uuid
import enum
from abceedapi import db, redis_store
from abceedapi.models import Base
from abceedapi.utils import json_utils
from flask_sqlalchemy import sqlalchemy


class QuestionType(enum.Enum):
    TOEIC_PART_1 = 1
    TOEIC_PART_2 = 1
    TOEIC_PART_3 = 3
    TOEIC_PART_4 = 4
    TOEIC_PART_5 = 5
    TOEIC_PART_6 = 6
    TOEIC_PART_7 = 7
    VOCABULARY_TEST = 8
    CONTENT = 9


class InteractionType(enum.Enum):
    TRUE_FALSE = 1
    CHOICE = 2
    FILL_IN = 3
    LONG_FILL_IN = 4
    MATCHING = 5
    PERFORMANCE = 6
    SEQUENCING = 7
    LIKERT = 8
    NUMERIC = 9
    NONE = 10
    OTHER = 11


class QuestionGroup(Base):
    __tablename__ = 'question_group'

    name = db.Column(db.String(255), nullable=False)

    test_id = db.Column(db.String(255), db.ForeignKey('test.id'))
    test = db.relationship(
        'Test', backref=db.backref('question_groups'))

    def __init__(self, test):
        self.test = test
        super(QuestionGroup, self).__init__()

    def to_dict(self, exclude=None, expand=False):
        qestion_group_dict = super(
            QuestionGroup, self).to_dict(exclude=exclude)

        if type(exclude) is list:
            exclude.append('question_group_id')
        else:
            exclude = ['question_group_id']

        qestion_group_dict['question_count'] = len(self.questions)
        qestion_group_dict['questions'] = []
        for question in self.questions:
            question_dict = question.to_dict(exclude=exclude, expand=False)
            qestion_group_dict['questions'].append(question_dict)

        if expand:
            qestion_group_dict['scripts'] = []
            for script in self.scripts:
                script_dict = script.to_dict(exclude=exclude)
                qestion_group_dict['scripts'].append(script_dict)

            qestion_group_dict['audios'] = []
            for audio in self.audios:
                audio_dict = audio.to_dict(exclude=exclude)
                qestion_group_dict['audios'].append(audio_dict)

            qestion_group_dict['explanations'] = []
            for explanation in self.explanations:
                explanation_dict = explanation.to_dict(exclude=exclude)
                qestion_group_dict['explanations'].append(explanation_dict)

            qestion_group_dict['explanation_audios'] = []
            for explanation_audio in self.explanation_audios:
                explanation_audio_dict = explanation_audio.to_dict(
                    exclude=exclude)
                qestion_group_dict['explanation_audios'].append(
                    explanation_audio_dict)

        return qestion_group_dict

    def load_json(self, json):
        questions = json.get('questions')
        if questions and type(questions) is list:
            self.questions = []
            for question_id in questions:
                question = Question.query.filter_by(id=question_id).first()
                self.questions.append(question)

        scripts = json.get('scripts')
        if scripts and type(scripts) is list:
            self.scripts = []
            for script_id in scripts:
                script = Script.query.filter_by(id=script_id).first()
                self.scripts.append(script)

        audios = json.get('audios')
        if audios and type(audios) is list:
            self.audios = []
            for audio_id in audios:
                audio = Audio.query.filter_by(id=audio_id).first()
                self.audios.append(audio)

        explanations = json.get('explanations')
        if explanations and type(explanations) is list:
            self.explanations = []
            for explanation_id in explanations:
                explanation = Explanation.query.filter_by(
                    id=explanation_id).first()
                self.explanations.append(explanation)

        explanation_audios = json.get('explanation_audios')
        if explanation_audios and type(explanation_audios) is list:
            self.explanation_audios = []
            for explanation_audio_id in explanation_audio_audios:
                explanation_audio = ExplanationAudio.query.filter_by(
                    id=explanation_audio_id).first()
                self.explanation_audios.append(explanation_audio)

        return super(QuestionGroup, self).load_json(json)


class Question(Base):
    __tablename__ = 'question'
    name = db.Column(db.String(255), nullable=False)

    # required
    question_type = db.Column(db.Integer, nullable=False)
    question_num = db.Column(db.String(50), nullable=True)
    question_index = db.Column(db.Integer, default=1, nullable=False)

    # extention
    section = db.Column(db.String(255), nullable=True)
    part = db.Column(db.String(255), nullable=True)
    label = db.Column(db.String(255), nullable=True)
    score = db.Column(db.Float, default=0.0, nullable=False)
    avg_score = db.Column(db.Float, default=0.0, nullable=False)
    level = db.Column(db.Integer, nullable=False)

    duration = db.Column(db.Float, nullable=True)
    avg_duration = db.Column(db.Float, nullable=True)

    candidates_count = db.Column(db.Integer, default=0, nullable=False)

    # qestionc activity params
    question_body = db.Column(db.Text, nullable=False)
    interaction_type = db.Column(db.Integer, nullable=False)
    correct_response = db.Column(db.Text, nullable=False)

    # rerations
    question_group_id = db.Column(
        db.String(255),
        db.ForeignKey('question_group.id'))
    question_group = db.relationship(
        'QuestionGroup', backref=db.backref('questions'))

    test_id = db.Column(db.String(255), db.ForeignKey('test.id'))
    test = db.relationship(
        'Test', backref=db.backref('questions'))

    __mapper_args__ = {
        "order_by": question_index
    }

    def __init__(self, test, question_type, question_body, interaction_type, correct_response):
        self.test = test
        self.question_type = question_type
        self.question_body = question_body
        self.interaction_type = interaction_type
        self.correct_response = correct_response
        super(Question, self).__init__()

    def __repr__(self):
        return '<Question %r>' % (self.id)

    def to_dict(self, exclude=None, expand=False):
        qestion_dict = super(Question, self).to_dict(exclude=exclude)

        if type(exclude) is list:
            exclude.append('question_id')
        else:
            exclude = ['question_id']

        qestion_dict['choice_count'] = len(self.choices)

        if expand:
            qestion_dict['choices'] = []
            for choices in self.choices:
                choices_dict = choices.to_dict(exclude=exclude)
                qestion_dict['choices'].append(choices_dict)

            qestion_dict['scripts'] = []
            for script in self.scripts:
                script_dict = script.to_dict(exclude=exclude)
                qestion_dict['scripts'].append(script_dict)

            qestion_dict['audios'] = []
            for audio in self.audios:
                audio_dict = audio.to_dict(exclude=exclude)
                qestion_dict['audios'].append(audio_dict)

            qestion_dict['explanations'] = []
            for explanation in self.explanations:
                explanation_dict = explanation.to_dict(exclude=exclude)
                qestion_dict['explanations'].append(explanation_dict)

            qestion_dict['explanation_audios'] = []
            for explanation_audio in self.explanation_audios:
                explanation_audio_dict = explanation_audio.to_dict(
                    exclude=exclude)
                qestion_dict['explanation_audios'].append(
                    explanation_audio_dict)

            qestion_dict['vocabularies'] = []
            for vocabulary in self.vocabularies:
                vocabulary_dict = vocabulary.to_dict(
                    exclude=exclude)
                qestion_dict['vocabularies'].append(
                    vocabulary_dict)

        return qestion_dict

    def load_json(self, json):
        choices = json.get('choices')
        if choices and type(choices) is list:
            self.choices = []
            for choice_id in choices:
                choice = Choice.query.filter_by(id=choice_id).first()
                self.choices.append(choice)

        scripts = json.get('scripts')
        if scripts and type(scripts) is list:
            self.scripts = []
            for script_id in scripts:
                script = Script.query.filter_by(id=script_id).first()
                self.scripts.append(script)

        audios = json.get('audios')
        if audios and type(audios) is list:
            self.audios = []
            for audio_id in audios:
                audio = Audio.query.filter_by(id=audio_id).first()
                self.audios.append(audio)

        explanations = json.get('explanations')
        if explanations and type(explanations) is list:
            self.explanations = []
            for explanation_id in explanations:
                explanation = Explanation.query.filter_by(
                    id=explanation_id).first()
                self.explanations.append(explanation)

        explanation_audios = json.get('explanation_audios')
        if explanation_audios and type(explanation_audios) is list:
            self.explanation_audios = []
            for explanation_audio_id in explanation_audio_audios:
                explanation_audio = ExplanationAudio.query.filter_by(
                    id=explanation_audio_id).first()
                self.explanation_audios.append(explanation_audio)

        return super(Question, self).load_json(json)


class Choice(Base):
    __tablename__ = 'question_choice'

    choice_label = db.Column(db.Text, nullable=False)
    choice_value = db.Column(db.String(255), nullable=False)

    question_id = db.Column(db.String(255), db.ForeignKey('question.id'))
    question = db.relationship(
        'Question', backref=db.backref('choices'))

    def __repr__(self):
        return '<Choice %r>' % (self.id)


class Script(Base):
    __tablename__ = 'question_script'

    script_body = db.Column(db.Text, nullable=False)
    script_explanation = db.Column(db.Text, nullable=True)
    priority = db.Column(db.Integer, default=128, nullable=False)
    sort_script = db.Column(db.Integer, nullable=True)

    question_id = db.Column(
        db.String(255), db.ForeignKey('question.id'))
    question = db.relationship(
        'Question', backref=db.backref('scripts'))

    question_group_id = db.Column(
        db.String(255), db.ForeignKey('question_group.id'))
    question_group = db.relationship(
        'QuestionGroup',
        backref=db.backref('scripts'))


class Audio(Base):
    __tablename__ = 'question_audio'

    standby_duration = db.Column(db.Float, default=0.0, nullable=False)
    sound_start_position = db.Column(db.Float, default=0.0, nullable=False)
    sound_end_position = db.Column(db.Float, nullable=True)
    priority = db.Column(db.Integer, default=128, nullable=False)

    resource_id = db.Column(
        db.String(255), db.ForeignKey('book_resource.id'))
    resource = db.relationship('BookResource')

    question_id = db.Column(
        db.String(255), db.ForeignKey('question.id'))
    question = db.relationship(
        'Question', backref=db.backref('audios'))

    question_group_id = db.Column(
        db.String(255),
        db.ForeignKey('question_group.id'))
    question_group = db.relationship(
        'QuestionGroup',
        backref=db.backref('audios'))


class Explanation(Base):
    __tablename__ = 'explanation'

    explanation_body = db.Column(db.Text, nullable=False)

    question_id = db.Column(db.String(255), db.ForeignKey('question.id'))
    question = db.relationship(
        'Question', backref=db.backref('explanations'))

    question_group_id = db.Column(
        db.String(255), db.ForeignKey('question_group.id'))
    question_group = db.relationship(
        'QuestionGroup',
        backref=db.backref('explanations'))


class ExplanationAudio(Base):
    __tablename__ = 'explanation_audio'

    sound_start_position = db.Column(db.Float, default=0.0, nullable=False)
    sound_end_position = db.Column(db.Float, nullable=True)
    priority = db.Column(db.Integer, default=128, nullable=False)

    resource_id = db.Column(
        db.String(255), db.ForeignKey('book_resource.id'))
    resource = db.relationship('BookResource')

    question_id = db.Column(db.String(255), db.ForeignKey('question.id'))
    question = db.relationship(
        'Question', backref=db.backref('explanation_audios'))

    question_group_id = db.Column(
        db.String(255), db.ForeignKey('question_group.id'))
    question_group = db.relationship(
        'QuestionGroup',
        backref=db.backref('explanation_audios'))


class Vocabulary(Base):
    __tablename__ = 'vocabulary'

    text_en = db.Column(db.Text, nullable=True)
    text_ja = db.Column(db.Text, nullable=True)
    sentence_en = db.Column(db.Text, nullable=True)
    sentence_ja = db.Column(db.Text, nullable=True)

    # Part of speech
    vocab_type = db.Column(db.String(255), nullable=True)

    question_id = db.Column(db.String(255), db.ForeignKey('question.id'))
    question = db.relationship(
        'Question', backref=db.backref('vocabularies'))


# Clear the Redis cache after data update
def receive_after_update(mapper, connection, target):
    redis_store.flushall()

sqlalchemy.event.listen(Question, "after_update", receive_after_update)
sqlalchemy.event.listen(QuestionGroup, "after_update", receive_after_update)
sqlalchemy.event.listen(Script, "after_update", receive_after_update)
sqlalchemy.event.listen(Explanation, "after_update", receive_after_update)
sqlalchemy.event.listen(Choice, "after_update", receive_after_update)
sqlalchemy.event.listen(Vocabulary, "after_update", receive_after_update)
sqlalchemy.event.listen(Audio, "after_update", receive_after_update)


def receive_after_delete(mapper, connection, target):
    redis_store.flushall()

sqlalchemy.event.listen(Question, "after_delete", receive_after_delete)
sqlalchemy.event.listen(QuestionGroup, "after_delete", receive_after_delete)
sqlalchemy.event.listen(Script, "after_delete", receive_after_delete)
sqlalchemy.event.listen(Explanation, "after_delete", receive_after_delete)
sqlalchemy.event.listen(Choice, "after_delete", receive_after_delete)
sqlalchemy.event.listen(Vocabulary, "after_delete", receive_after_delete)
sqlalchemy.event.listen(Audio, "after_delete", receive_after_delete)
