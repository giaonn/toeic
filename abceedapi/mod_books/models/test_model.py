# coding: utf-8

import uuid, enum
from abceedapi import db
from abceedapi.models import Base
from abceedapi.utils import json_utils


class Test(Base):
    __tablename__ = 'test'

    test_index = db.Column(db.Integer, default=1, nullable=False)
    name = db.Column(db.String(255), nullable=False)

    book_id = db.Column(db.String(255), db.ForeignKey('book.id'))
    book = db.relationship('Book', backref=db.backref('tests'))

    analytics_test_id = db.Column(db.String(255), nullable=False)

    __mapper_args__ = {
        "order_by": test_index
    }

    def __init__(self, name, book):
        self.name = name
        self.book = book
        super(Test, self).__init__()

    def __repr__(self):
        return '<Test %r>' % (self.id)

    def to_dict(self, exclude=None, expand=False):
        test_dict = super(Test, self).to_dict(exclude=exclude)

        if type(exclude) is list and 'test_id' not in exclude:
            exclude.append('test_id')
        else:
            exclude = ['test_id']

        test_dict['question_count'] = len(filter(lambda x: x.question_type != 9, self.questions))
        test_dict['questions'] = []
        part_list = {}
        for question in sorted(self.questions, key=lambda k: k.question_index):
            question_dict = question.to_dict(exclude=exclude, expand=expand)
            test_dict['questions'].append(question_dict)
            if question.part not in part_list:
                part_list[question.part] = [question.id]
            else:
                part_list[question.part].append(question.id)
        part_list = map(lambda x: { 'name': x, 'questions': part_list[x] }, part_list)
        part_list = sorted(part_list, key=lambda k: k['name'])
        test_dict['part_list'] = part_list

        test_dict['question_group_count'] = len(self.question_groups)
        test_dict['question_groups'] = []
        for question_group in self.question_groups:
            question_group_dict = question_group.to_dict(exclude=exclude, expand=expand)
            test_dict['question_groups'].append(question_group_dict)

        test_dict['question_group_count'] = len(self.question_groups)
        test_dict['question_groups'] = []
        for question_group in self.question_groups:
            question_group_dict = question_group.to_dict(exclude=exclude, expand=expand)
            test_dict['question_groups'].append(question_group_dict)

        stat = self.stats.first()
        if stat:
            test_dict['analytics'] = stat.to_dict(exclude=exclude)
        else:
            test_dict['analytics'] = None

        return test_dict

