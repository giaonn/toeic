# coding: utf-8

import uuid
import enum
from abceedapi import db, redis_store
from abceedapi.models import Base
from flask_sqlalchemy import sqlalchemy



class BookType(enum.Enum):
    TOEIC_TEST = 1
    VOCABULARY_TEST = 2


class Publisher(Base):
    __tablename__ = 'book_publisher'

    name = db.Column(db.String(255), nullable=False)

    def __init__(self, name):
        self.name = name
        super(Publisher, self).__init__()

    def __repr__(self):
        return '<Publisher %r>' % (self.id)

categories_table = db.Table(
    'book_categories',
    db.Column('book_category_id', db.String(255), db.ForeignKey('book_category.id')),
    db.Column('book_id', db.String(255), db.ForeignKey('book.id')),
    db.Column('sort_num',db.Integer,nullable=True)
)

authors_table = db.Table(
    'book_authors',
    db.Column('book_author_id', db.String(255), db.ForeignKey('book_author.id')),
    db.Column('book_id', db.String(255), db.ForeignKey('book.id'))
)


class BookCategory(Base):
    __tablename__ = 'book_category'

    name = db.Column(db.String(255), nullable=False)

    def __init__(self, name):
        self.name = name
        super(BookCategory, self).__init__()

    def __repr__(self):
        return '<BookCategory %r>' % (self.id)


class Author(Base):
    __tablename__ = 'book_author'

    name = db.Column(db.String(255), nullable=False)
    avatar_image_url = db.Column(db.Text, nullable=True)

    def __init__(self, name):
        self.name = name
        super(Author, self).__init__()

    def __repr__(self):
        return '<Author %r>' % (self.id)


class Book(Base):
    __tablename__ = 'book'

    # book_type and title is requiered
    title = db.Column(db.String(255), nullable=False)
    book_type = db.Column(db.Integer, nullable=False)
    cover_image_url = db.Column(db.Text, nullable=True)

    is_public = db.Column(db.Boolean, default=False, nullable=False)
    is_removed = db.Column(db.Boolean, default=False, nullable=False)
    is_free = db.Column(db.Boolean, default=False, nullable=False)
    is_recommended = db.Column(db.Boolean, default=False, nullable=False)
    price = db.Column(db.Float, default=0.0, nullable=False)

    sort_num =  db.Column(db.Integer, nullable=False)

    book_publisher_id = db.Column(
        db.String(255), db.ForeignKey('book_publisher.id'))
    book_publisher = db.relationship('Publisher',
                                     backref=db.backref('books', lazy='dynamic'))

    # book_author_id = db.Column(db.String(255), db.ForeignKey('book_author.id'))
    # book_author = db.relationship('Autho  r',
    #                               backref=db.backref('books', lazy='dynamic'))

    authors = db.relationship(
        'Author',
        secondary=authors_table,
        backref=db.backref('books', lazy='dynamic'))

    categories = db.relationship(
        'BookCategory',
        secondary=categories_table,
        backref=db.backref('books', lazy='dynamic'))

    googleplaystore_item = db.Column(db.String(255), nullable=True)
    appstore_item = db.Column(db.String(255), nullable=True)

    # New instance instantiation procedure
    def __init__(self, title, book_type):
        self.title = title
        self.book_type = book_type
        super(Book, self).__init__()

    def __repr__(self):
        return '<Book %r>' % (self.id)


    def to_dict_v2(self, exclude=None, expand=False, show_contents=False):
        book_dict = super(Book, self).to_dict(exclude=exclude)
        if type(exclude) is list and 'book_id' not in exclude:
            exclude.append('book_id')
        else:
            exclude = ['book_id']

        if not show_contents:
            exclude.append('question_body')

        book_dict['test_count'] = len(self.tests)

        book_dict['authors'] = []
        for author in self.authors:
            author_dict = author.to_dict(exclude=exclude)
            book_dict['authors'].append(author_dict)

        if expand:
            book_dict['tests'] = []
            for test in self.tests:
                test_dict = test.to_dict(exclude=exclude, expand=show_contents)
                book_dict['tests'].append(test_dict)

        if show_contents:
            book_dict['resources'] = []
            for resource in self.resources:
                resource_dict = resource.to_dict(exclude=exclude)
                book_dict['resources'].append(resource_dict)

            book_dict['packages'] = []
            for package in self.packages:
                package_dict = package.to_dict(exclude=exclude)
                book_dict['packages'].append(package_dict)

        return book_dict

    def to_dict(self, exclude=None, expand=False, show_contents=False):
        book_dict = super(Book, self).to_dict(exclude=exclude)

        if type(exclude) is list and 'book_id' not in exclude:
            exclude.append('book_id')
        else:
            exclude = ['book_id']

        if not show_contents:
            exclude.append('question_body')

        book_dict['test_count'] = len(self.tests)
        book_dict['categories'] = []
        for category in self.categories:
            category_dict = category.to_dict(exclude=exclude)
            book_dict['categories'].append(category_dict)

        book_dict['authors'] = []
        for author in self.authors:
            author_dict = author.to_dict(exclude=exclude)
            book_dict['authors'].append(author_dict)

        if expand:
            book_dict['tests'] = []
            for test in self.tests:
                test_dict = test.to_dict(exclude=exclude, expand=show_contents)
                book_dict['tests'].append(test_dict)

        if show_contents:
            book_dict['resources'] = []
            for resource in self.resources:
                resource_dict = resource.to_dict(exclude=exclude)
                book_dict['resources'].append(resource_dict)

            book_dict['packages'] = []
            for package in self.packages:
                package_dict = package.to_dict(exclude=exclude)
                book_dict['packages'].append(package_dict)

        return book_dict


    def load_json(self, json):
        authors = json.get('authors')
        if authors and type(authors) is list:
            self.authors = []
            for author_id in authors:
                author = Author.query.filter_by(id=author_id).first()
                self.authors.append(author)

        categories = json.get('categories')
        if categories and type(categories) is list:
            self.categories = []
            for category_id in categories:
                category = BookCategory.query.filter_by(id=category_id).first()
                self.categories.append(category)

        return super(Book, self).load_json(json)


class BookResourceType(enum.Enum):
    AUDIO = 1
    IMAGE = 2
    MOVIE = 3


class BookResource(Base):
    __tablename__ = 'book_resource'

    resource_type = db.Column(db.Integer, nullable=False)
    filename = db.Column(db.String(255), nullable=False)
    extension = db.Column(db.String(255), nullable=False)
    s3_bucket = db.Column(db.Text, nullable=False)
    s3_key = db.Column(db.Text, nullable=False)
    s3_url = db.Column(db.Text, nullable=True)

    book_id = db.Column(db.String(255), db.ForeignKey('book.id'))
    book = db.relationship('Book', backref=db.backref('resources'))

    __mapper_args__ = {
        "order_by": 'date_modified'
    }

    def __repr__(self):
        return '<BookResource {}'.format(self.id)

    def __init__(self, book, resource_type, filename, extension, s3_bucket, s3_key):
        self.book = book
        self.resource_type = resource_type
        self.filename = filename
        self.extension = extension
        self.s3_bucket = s3_bucket
        self.s3_key = s3_key
        super(BookResource, self).__init__()


class BookPackage(Base):
    __tablename__ = 'book_package'

    build_number = db.Column(db.Integer, nullable=False, default=1)
    s3_bucket = db.Column(db.Text, nullable=False)
    s3_key = db.Column(db.Text, nullable=False)
    s3_url = db.Column(db.Text, nullable=True)
    aes_key = db.Column(db.Text, nullable=True)
    encrypted_aes_key = db.Column(db.Text, nullable=True)

    book_id = db.Column(db.String(255), db.ForeignKey('book.id'))
    book = db.relationship('Book', backref=db.backref('packages', lazy='dynamic'))

    def __repr__(self):
        return '<BookPackage {}'.format(self.id)

    def __init__(self, book, s3_bucket, s3_key):
        self.book = book
        self.s3_bucket = s3_bucket
        self.s3_key = s3_key
        super(BookPackage, self).__init__()


def receive_after_update(mapper, connection, target):
    redis_store.flushall()

sqlalchemy.event.listen(Book, "after_update", receive_after_update)
sqlalchemy.event.listen(BookResource, "after_update", receive_after_update)
sqlalchemy.event.listen(BookPackage, "after_update", receive_after_update)


def receive_after_delete(mapper, connection, target):
    redis_store.flushall()

sqlalchemy.event.listen(Book, "after_delete", receive_after_delete)
sqlalchemy.event.listen(BookResource, "after_delete", receive_after_delete)
sqlalchemy.event.listen(BookPackage, "after_delete", receive_after_delete)
