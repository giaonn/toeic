# coding: utf-8
import json
from flask import Blueprint

mod_books = Blueprint('books', __name__, url_prefix='/books')

from . import (
    book_controllers, question_controllers, test_controllers,
    author_controllers, category_controllers, publisher_controllers
)

