# coding: utf-8

import json
import pprint
from flask import Blueprint, request, jsonify, abort
from flask_sqlalchemy import sqlalchemy
from abceedapi import db, logger, redis_store
from abceedapi.utils.auth_utils import cognito_auth_required
from abceedapi.mod_books.models import Book, BookCategory, Test, Question, BookType, Author, Publisher, BookResource, BookPackage, Script
from . import mod_books
from sqlalchemy import desc
import base64
from Crypto import Random
from Crypto.Cipher import AES
# mod_books = Blueprint('books', __name__, url_prefix='/books')
import MySQLdb
from operator import itemgetter
import copy

class AESCipher(object):
    def __init__(self, key, block_size=32):
        self.bs = block_size
        if len(key) >= len(str(block_size)):
            self.key = key[:block_size]
        else:
            self.key = self._pad(key)

    def encrypt(self, raw):
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:]))

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    def _unpad(self, s):
        return s[:-ord(s[len(s)-1:])]


@mod_books.route('/', methods=['GET'])
def list_book():
    print "get list book"

    books = Book.query.filter_by(is_removed=False).filter_by(is_public=True)

    category = request.args.get('category')

    if category:
        print 'iamin'
        books = books.filter(Book.categories.any(name=category))

    result = []

    for book in books:
        a = book.to_dict_v2()
        book_id = "'" + book.id + "'"
        sql = 'select BC.date_created, BC.date_modified, BC.id as catId, BC.name,BCS.sort_num,B.id as bookId from book B ' \
              + ' left join book_categories BCS on B.id = BCS.book_id ' \
              + ' left join book_category BC on BCS.book_category_id = BC.id where B.id = ' + book_id + ' '

        if category:
            nameCat    = "'" + category + "'"
            getCatCm   = 'select id from book_category where name = ' + nameCat
            myCategory = db.engine.execute(getCatCm)
            idCat = ''
            for item in myCategory:
                idCat = item[0]
            idCat = "'" + idCat + "'"
            sql = sql + ' and BC.id = ' + idCat

        res = db.engine.execute(sql)
        a['categories'] = []
        dict = {}
        for row in res:

            dict['date_created']  = row[0]
            dict['date_modified'] = row[1]
            dict['id']            = row[2]
            dict['name']          = row[3]
            dict['sort_num']      = row[4]
            if row[2]:
                a['categories'].append(copy.copy(dict))
            a['pos'] = row[4]

        result.append(a)

    if category:
        result = sorted(result, key=lambda k: k['pos'])

    return jsonify(result)


@mod_books.route('/', methods=['POST'])
def create_book():
    print "create book"
    req_json = request.json if request.json else {}

    title = req_json.get('title')
    book_type = req_json.get('book_type')

    if not title or not book_type:
        abort(400, {'message': 'book title and book_type is requeired.'})

    book = Book(title=title, book_type=book_type)
    book.load_json(req_json)

    db.session.add(book)
    db.session.commit()

    return jsonify(book.to_dict(expand=True))


@mod_books.route('/recommend', methods=['GET'])
def list_recommend_book():
    print "LOG"
    books = Book.query.filter_by(is_removed=False).filter_by(is_public=True).filter_by(is_recommended=True)
    result = []
    for book in books:
        result.append(book.to_dict())
    result = sorted(result, key=lambda k: k['sort_num'])
    return jsonify(result)


@mod_books.route('/_search', methods=['GET'])
@cognito_auth_required()
def search_book(user=None):
    print "LOG"
    books = Book.query.filter_by(is_removed=False)
    if not user or not user.is_admin:
        books = books.filter_by(is_public=True)

    title = request.args.get('title')
    name = request.args.get('name')
    if title:
        books = books.filter(Book.title.like(u'%{}%'.format(title)))
    elif name:
        books = books.filter(Book.title.like(u'%{}%'.format(name)))
    else:
        books = []

    result = []
    for book in books:
        result.append(book.to_dict())

    return jsonify(result)


@mod_books.route('/<book_id>', methods=['GET'])
def get_book(book_id=None):
    # book = Book.query.filter_by(id=book_id).first()
    print "LOG"
    book = db.session.query(Book).options(
        sqlalchemy.orm.subqueryload('book_publisher'),
        sqlalchemy.orm.subqueryload('categories'),
        sqlalchemy.orm.subqueryload('tests').subqueryload('questions'),
        sqlalchemy.orm.subqueryload('tests').subqueryload('question_groups')
    ).filter_by(id=book_id).first()
    if book:
        book_dict = book.to_dict(expand=True)
        return jsonify(book_dict)
    abort(404, {'message': 'book cannot be found.'})


@mod_books.route('/<book_id>', methods=['PUT'])
def update_book(book_id):
    print "LOG"
    book = Book.query.filter_by(id=book_id).first()
    req_json = request.json if request.json else {}
    if book:
        book = book.load_json(req_json)
        db.session.commit()
        return jsonify(book.to_dict(expand=True))
    abort(404, {'message': 'book cannot be found.'})


@mod_books.route('/<book_id>', methods=['DELETE'])
def remove_book(book_id):
    print "LOG"
    book = Book.query.filter_by(id=book_id).first()
    if book:
        book.is_removed = True
        db.session.commit()
        return jsonify(book.to_dict(expand=True))
    abort(404, {'message': 'book cannot be found.'})


@mod_books.route('/<book_id>/_download', methods=['GET'])
# @cognito_auth_required()
def download_book(book_id=None, user=None):
    print "LOG"

    # if request.args.get('publisher') and redis_store.exists(book_id + '/_download?publisher=True'):
    #     return redis_store.get(book_id + '/_download?publisher=True'), 200, {'Content-Type': 'application/json'}
    # elif redis_store.exists(book_id + '/_download'):
    #     return redis_store.get(book_id + '/_download'), 200, {'Content-Type': 'application/json'}
    book = db.session.query(Book).options(
        sqlalchemy.orm.subqueryload('resources'),
        sqlalchemy.orm.subqueryload('tests').subqueryload('questions').subqueryload('choices'),
        sqlalchemy.orm.subqueryload('tests').subqueryload('questions').subqueryload('explanations'),
        sqlalchemy.orm.subqueryload('tests').subqueryload('questions').subqueryload('audios'),
        sqlalchemy.orm.subqueryload('tests').subqueryload('questions').subqueryload('explanation_audios'),
        sqlalchemy.orm.subqueryload('tests').subqueryload('questions').subqueryload('scripts'),
        sqlalchemy.orm.subqueryload('tests').subqueryload('question_groups').subqueryload('explanations'),
        sqlalchemy.orm.subqueryload('tests').subqueryload('question_groups').subqueryload('audios'),
        sqlalchemy.orm.subqueryload('tests').subqueryload('question_groups').subqueryload('explanation_audios'),
        sqlalchemy.orm.subqueryload('tests').subqueryload('question_groups').subqueryload('scripts')
    ).filter_by(id=book_id).first()
    # purchased = user.purchase_items.filter_by(book_id=book_id).first()

    print request.args.get('publisher')
    purchased = True
    if not book:
        abort(404, {'message': 'book cannot be found.'})
    if request.args.get('publisher'):
        # TODO: Check CognitoUserPoolID (Is it for administrator?)
        redis_store.set(book_id + '/_download?publisher=True', json.dumps(book.to_dict(expand=True, show_contents=True)))
        return jsonify(book.to_dict(expand=True, show_contents=True))
    if book and purchased:
        data = json.dumps(book.to_dict(expand=True, show_contents=True))
        cipher = AESCipher("PkDv17c6xxiqXPrvG2cUiq90VIrERfthHuViKapv6E1F7E0IgP")
        data = cipher.encrypt(data)
        package = book.packages.first()
        package_url = package.s3_url if package else ''
        redis_store.set(book_id + '/_download', json.dumps({'data': data, 'package_url': package_url, 'key': cipher.key}))
        return jsonify({'data': data, 'package_url': package_url, 'key': cipher.key})
    abort(403, {'message': 'user is not allowed to download content.'})


@mod_books.route('/<book_id>/resources', methods=['POST'])
def create_book_resource(book_id=None):
    print "LOG"
    req_json = request.json if request.json else {}

    resource_type = req_json.get('resource_type')
    filename = req_json.get('filename')
    extension = req_json.get('extension')
    s3_bucket = req_json.get('s3_bucket')
    s3_key = req_json.get('s3_key')

    if not resource_type:
        abort(400, {'message': 'resource_type is requeired.'})
    if not filename:
        abort(400, {'message': 'filename is requeired.'})
    if not extension:
        abort(400, {'message': 'extension is requeired.'})
    if not s3_bucket:
        abort(400, {'message': 's3_bucket is requeired.'})
    if not s3_key:
        abort(400, {'message': 's3_key is requeired.'})

    book = Book.query.filter_by(id=book_id).first()
    if book:
        resource = BookResource(
            book=book,
            resource_type=resource_type,
            filename=filename,
            extension=extension,
            s3_bucket=s3_bucket,
            s3_key=s3_key
        )
        resource.load_json(req_json)
        db.session.add(resource)
        db.session.commit()
        return jsonify(resource.to_dict())
    abort(404, {'message': 'book cannot be found.'})


@mod_books.route('/resources/<resource_id>', methods=['DELETE'])
def delete_book_resource(resource_id=None):
    print "LOG"
    resource = BookResource.query.filter_by(id=resource_id).first()
    if resource:
        try:
            db.session.delete(resource)
            db.session.commit()
        except sqlalchemy.exc.IntegrityError:
            abort(400, {'message': 'book_resource (audio) already in use.'})

        return jsonify(resource.to_dict())
    abort(404, {'message': 'book_resource cannot be found.'})


@mod_books.route('/<book_id>/packages', methods=['POST'])
def create_book_package(book_id=None):
    print "LOG"
    req_json = request.json if request.json else {}

    s3_bucket = req_json.get('s3_bucket')
    s3_key = req_json.get('s3_key')

    if not s3_bucket:
        abort(400, {'message': 's3_bucket is requeired.'})
    if not s3_key:
        abort(400, {'message': 's3_key is requeired.'})
    if not s3_url:
        abort(400, {'message': 's3_url is requeired.'})

    book = Book.query.filter_by(id=book_id).first()
    if book:
        package = BookPackage(
            book=book,
            s3_bucket=s3_bucket,
            s3_key=s3_key
        )
        package.load_json(req_json)
        db.session.add(package)
        db.session.commit()
        return jsonify(package.to_dict())
    abort(404, {'message': 'book cannot be found.'})


@mod_books.route('/packages/<package_id>', methods=['DELETE'])
def delete_book_package(package_id=None):
    print "LOG"
    package = BookPackage.query.filter_by(id=package_id).first()
    if package:
        db.session.delete(package)
        db.session.commit()
        return jsonify(package.to_dict())
    abort(404, {'message': 'book_package cannot be found.'})
