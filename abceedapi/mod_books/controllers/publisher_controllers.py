# coding: utf-8

import json
from flask import Blueprint, request, jsonify, abort
from abceedapi import db, logger
from abceedapi.mod_books.models import Book, BookCategory, Test, Question, BookType, Author, Publisher
from . import mod_books


@mod_books.route('/publishers', methods=['GET'])
def list_publishers():
    publishers = Publisher.query.all()
    result = []
    for publisher in publishers:
        result.append(publisher.to_dict())
    return jsonify(result)


@mod_books.route('/publishers', methods=['POST'])
def create_publisher():
    req_json = request.json if request.json else {}

    name = req_json.get('name')
    if not name:
        abort(400, {'message': 'publisher name is requeired.'})

    publisher = Publisher(name=name)
    publisher.load_json(req_json)
    db.session.add(publisher)
    db.session.commit()
    return jsonify(publisher.to_dict())


@mod_books.route('/publishers/<publisher_id>', methods=['DELETE'])
def delete_publisher(publisher_id):
    publisher = Publisher.query.filter_by(id=publisher_id).first()
    if publisher:
        db.session.delete(publisher)
        db.session.commit()
        return jsonify(publisher.to_dict())
    abort(404, {'message': 'publisher cannot be found.'})
