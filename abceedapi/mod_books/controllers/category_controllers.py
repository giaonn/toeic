# coding: utf-8

import json
from flask import Blueprint, request, jsonify, abort
from abceedapi import db, logger
from abceedapi.mod_books.models import Book, BookCategory, Test, Question, BookType, Author, Publisher
from . import mod_books


@mod_books.route('/categories', methods=['GET'])
def list_book_categories():
    categories = BookCategory.query.all()
    result = []
    for categoriy in categories:
        result.append(categoriy.to_dict())
    return jsonify(result)


@mod_books.route('/categories', methods=['POST'])
def create_book_category():
    req_json = request.json if request.json else {}

    name = req_json.get('name')
    if not name:
        abort(400, {'message': 'category name is requeired.'})

    category = BookCategory(name=name)
    category.load_json(req_json)
    db.session.add(category)
    db.session.commit()
    return jsonify(category.to_dict())


@mod_books.route('/categories/<category_id>', methods=['DELETE'])
def delete_book_category(category_id):
    category = BookCategory.query.filter_by(id=category_id).first()
    if category:
        db.session.delete(category)
        db.session.commit()
        return jsonify(category.to_dict())
    abort(404, {'message': 'category cannot be found.'})
