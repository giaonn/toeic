# coding: utf-8

import json
from flask import Blueprint, request, jsonify, abort
from abceedapi import db, logger
from abceedapi.mod_books.models import Book, BookCategory, Test, Question, BookType, Author, Publisher
from . import mod_books


@mod_books.route('/authors', methods=['GET'])
def list_authors():
    authors = Author.query.all()
    result = []
    for author in authors:
        result.append(author.to_dict())
    return jsonify(result)


@mod_books.route('/authors', methods=['POST'])
def create_author():
    req_json = request.json if request.json else {}

    name = req_json.get('name')
    if not name:
        abort(400, {'message': 'author name is requeired.'})

    author = Author(name=name)
    author.load_json(req_json)
    db.session.add(author)
    db.session.commit()
    return jsonify(author.to_dict())


@mod_books.route('/authors/<author_id>', methods=['DELETE'])
def delete_author(author_id):
    author = Author.query.filter_by(id=author_id).first()
    if author:
        db.session.delete(author)
        db.session.commit()
        return jsonify(author.to_dict())
    abort(404, {'message': 'author cannot be found.'})
