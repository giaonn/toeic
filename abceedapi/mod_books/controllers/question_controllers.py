# coding: utf-8

import json
from flask import Blueprint, request, jsonify, abort
from abceedapi import db, logger
from abceedapi.mod_books.models import Test, Question, InteractionType, Choice, QuestionGroup, Script, Audio, Explanation, ExplanationAudio
from . import mod_books


@mod_books.route('/tests/<test_id>/questions', methods=['POST'])
def create_question(test_id=None):
    req_json = request.json if request.json else {}

    question_type = req_json.get('question_type')
    question_body = req_json.get('question_body')
    interaction_type = req_json.get('interaction_type')
    correct_response = req_json.get('correct_response')

    if not question_type:
        abort(400, {'message': 'question_type is requeired.'})
    if not question_body:
        abort(400, {'message': 'question_body is requeired.'})
    if not interaction_type:
        abort(400, {'message': 'interaction_type is requeired.'})
    if not correct_response:
        abort(400, {'message': 'correct_response is requeired.'})

    test = Test.query.filter_by(id=test_id).first()
    if test:
        question = Question(
            test=test,
            question_type=question_type,
            question_body=question_body,
            interaction_type=interaction_type,
            correct_response=correct_response)
        question.load_json(req_json)
        db.session.add(question)
        db.session.commit()
        return jsonify(question.to_dict(expand=True))
    abort(404, {'message': 'test cannot be found.'})


@mod_books.route('/questions/<question_id>', methods=['PUT', 'GET'])
def update_question(question_id=None):
    req_json = request.json if request.json else {}
    question = Question.query.filter_by(id=question_id).first()
    if not question:
        abort(404, {'message': 'question cannot be found.'})
    elif request.method == 'PUT':
        question = question.load_json(req_json)
        db.session.commit()
        return jsonify(question.to_dict(expand=True))
    else:
        return jsonify(question.to_dict(expand=True))


@mod_books.route('/questions/<question_id>/choices', methods=['POST'])
def create_choice(question_id=None):
    req_json = request.json if request.json else {}
    question = Question.query.filter_by(id=question_id).first()
    if question:
        choice = Choice()
        choice.question = question
        choice.load_json(req_json)
        db.session.add(choice)
        db.session.commit()
        return jsonify(choice.to_dict())
    abort(404, {'message': 'question cannot be found.'})


@mod_books.route('/tests/<test_id>/question_groups', methods=['POST'])
def create_question_group(test_id=None):
    req_json = request.json if request.json else {}

    test = Test.query.filter_by(id=test_id).first()
    if test:
        group = QuestionGroup(test=test)
        group.load_json(req_json)
        db.session.add(group)
        db.session.commit()
        return jsonify(group.to_dict(expand=True))
    abort(404, {'message': 'test cannot be found.'})


@mod_books.route('/question_groups/<question_groups_id>', methods=['PUT'])
def update_question_group(question_groups_id=None):
    req_json = request.json if request.json else {}

    group = QuestionGroup.query.filter_by(id=question_groups_id).first()
    if group:
        group.load_json(req_json)
        db.session.commit()
        return jsonify(group.to_dict(expand=True))
    abort(404, {'message': 'question_group cannot be found.'})


@mod_books.route('/choices/<choice_id>', methods=['PUT', 'DELETE'])
def modify_choice(choice_id):
    req_json = request.json if request.json else {}

    choice = Choice.query.filter_by(id=choice_id).first()
    if not choice:
        abort(404, {'message': 'choice cannot be found.'})
    elif request.method == 'PUT':
        choice = choice.load_json(req_json)
    elif request.method == 'DELETE':
        db.session.delete(choice)
    db.session.commit()
    return jsonify(choice.to_dict())
    abort(500, {'message': 'internal server error.'})


@mod_books.route('/scripts', methods=['POST'])
def create_script():
    req_json = request.json if request.json else {}
    script = Script()
    script.load_json(req_json)
    db.session.add(script)
    db.session.commit()
    return jsonify(script.to_dict())


@mod_books.route('/scripts/<script_id>', methods=['PUT', 'DELETE'])
def modify_script(script_id):
    req_json = request.json if request.json else {}

    script = Script.query.filter_by(id=script_id).first()
    if not script:
        abort(404, {'message': 'script cannot be found.'})
    elif request.method == 'PUT':
        script = script.load_json(req_json)
    elif request.method == 'DELETE':
        db.session.delete(script)
    db.session.commit()
    return jsonify(script.to_dict())
    abort(500, {'message': 'internal server error.'})


@mod_books.route('/audios', methods=['POST'])
def create_audio():
    req_json = request.json if request.json else {}
    audio = Audio()
    audio.load_json(req_json)
    db.session.add(audio)
    db.session.commit()
    return jsonify(audio.to_dict())


@mod_books.route('/audios/<audio_id>', methods=['PUT', 'DELETE'])
def modify_audio(audio_id):
    req_json = request.json if request.json else {}

    audio = Audio.query.filter_by(id=audio_id).first()
    if not audio:
        abort(404, {'message': 'audio cannot be found.'})
    elif request.method == 'PUT':
        audio = audio.load_json(req_json)
    elif request.method == 'DELETE':
        db.session.delete(audio)
    db.session.commit()
    return jsonify(audio.to_dict())
    abort(500, {'message': 'internal server error.'})


@mod_books.route('/explanations', methods=['POST'])
def create_explanation():
    req_json = request.json if request.json else {}
    explanation = Explanation()
    explanation.load_json(req_json)
    db.session.add(explanation)
    db.session.commit()
    return jsonify(explanation.to_dict())


@mod_books.route('/explanations/<explanation_id>', methods=['PUT', 'DELETE'])
def modify_explanation(explanation_id):
    req_json = request.json if request.json else {}

    explanation = Explanation.query.filter_by(id=explanation_id).first()
    if not explanation:
        abort(404, {'message': 'explanation cannot be found.'})
    elif request.method == 'PUT':
        explanation = explanation.load_json(req_json)
    elif request.method == 'DELETE':
        db.session.delete(explanation)
    db.session.commit()
    return jsonify(explanation.to_dict())
    abort(500, {'message': 'internal server error.'})


@mod_books.route('/explanation_audios', methods=['POST'])
def create_explanation_audio():
    req_json = request.json if request.json else {}
    exp_audio = ExplanationAuido()
    exp_audio.load_json(req_json)
    db.session.add(exp_audio)
    db.session.commit()
    return jsonify(exp_audio.to_dict())


