# coding: utf-8

import json
from flask import Blueprint, request, jsonify, abort
from abceedapi import db, logger
from abceedapi.mod_books.models import Book, Test
from . import mod_books


@mod_books.route('/<book_id>/tests', methods=['GET'])
def list_test(book_id=None):
    tests = Test.query.filter_by(book_id=book_id).all()
    result = []
    for test in tests:
        result.append(test.to_dict())
    return jsonify(result)


@mod_books.route('/<book_id>/tests', methods=['POST'])
def create_test(book_id=None):
    req_json = request.json if request.json else {}

    name = req_json.get('name')
    if not name:
        abort(400, {'message': 'test name is requeired.'})

    book = Book.query.filter_by(id=book_id).first()
    if book:
        test = Test(name, book)
        test.load_json(req_json)
        db.session.add(test)
        db.session.commit()
        return jsonify(book.to_dict(expand=True))
    abort(404, {'message': 'book cannot be found.'})



@mod_books.route('/tests/<test_id>', methods=['GET'])
def get_test(test_id):
    test = Test.query.filter_by(id=test_id).first()
    if test:
        return jsonify(test.to_dict(expand=True))
    abort(404, {'message': 'test cannot be found.'})



@mod_books.route('/tests/<test_id>', methods=['PUT'])
def update_test(test_id):
    test = Test.query.filter_by(id=test_id).first()
    req_json = request.json if request.json else {}
    if test:
        test = test.load_json(req_json)
        db.session.commit()
        return jsonify(test.to_dict(expand=True))
    abort(404, {'message': 'test cannot be found.'})
