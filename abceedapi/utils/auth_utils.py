# coding: utf-8

import json
import datetime
from functools import wraps

from flask import abort, request
from abceedapi.mod_users.models import User
from abceedapi.config import TEST_USER_ID, DEBUG


def cognito_auth_required(publisher=False):
    def decorator(func):
        @wraps(func)
        def with_auth_wrapper(*args, **kwargs):
            if DEBUG == True:
                user = User.query.filter_by(id=TEST_USER_ID).first()
                if not user:
                    abort(404, {'message': 'user cannot be found. [DEBUG=True]'})
                return func(user=user, *args, **kwargs)

            remote_user = request.remote_user
            if not remote_user or type(remote_user) is not dict:
                abort(401, {'message': 'unauthorized.'})

            try:
                cognito_pool_id = remote_user['cognitoIdentityPoolId']
                cognito_user_id = remote_user['cognitoIdentityId']
                auth_type = remote_user['cognitoAuthenticationType']
            except:
                abort(401, {'message': 'unauthorized.'})

            user = User.query.filter_by(
                cognito_pool_id=cognito_pool_id,
                cognito_user_id=cognito_user_id).first()

            if not user:
                abort(404, {'message': 'user cannot be found.'})

            return func(user=user, *args, **kwargs)

        return with_auth_wrapper
    return decorator

def cognito_auth_option(publisher=False):
    def decorator(func):
        @wraps(func)
        def with_auth_wrapper(*args, **kwargs):
            if DEBUG == True:
                user = User.query.filter_by(id=TEST_USER_ID).first()
                if not user:
                    abort(404, {'message': 'user cannot be found. [DEBUG=True]'})
                return func(user=user, *args, **kwargs)

            remote_user = request.remote_user
            if not remote_user or type(remote_user) is not dict:
                abort(401, {'message': 'unauthorized.'})

            try:
                cognito_pool_id = remote_user['cognitoIdentityPoolId']
                cognito_user_id = remote_user['cognitoIdentityId']
                auth_type = remote_user['cognitoAuthenticationType']
            except:
                abort(401, {'message': 'unauthorized.'})

            user = User.query.filter_by(
                cognito_pool_id=cognito_pool_id,
                cognito_user_id=cognito_user_id).first()

            if not user:
                return func(user=None, *args, **kwargs)

            return func(user=user, *args, **kwargs)

        return with_auth_wrapper
    return decorator