# coding: utf-8

import re
import json
import datetime


def datetime_converter(value):
    return datetime.datetime.strftime(value, '%Y-%m-%dT%H:%M:%SZ')

def datetime_parser(value):
    return datetime.datetime.strptime(value, '%Y-%m-%dT%H:%M:%SZ')


def model2dict(inst, cls, exclude=None):
    convert = dict()
    convert['DATETIME'] = datetime_converter
    id_patter = re.compile('.*_id$')
    exclude = exclude if exclude else []

    d = dict()
    for c in cls.__table__.columns:
        v = getattr(inst, c.name)
        typestr = str(c.type)

        if c.name in exclude:
            continue

        if typestr in convert.keys() and v is not None:
            try:
                d[c.name] = convert[typestr](v)
            except:
                d[c.name] = "Error:  Failed to covert using ", str(
                    convert[c.type])
        elif v is None:
            d[c.name] = str()
        else:
            d[c.name] = v

        if id_patter.match(c.name) and hasattr(inst, c.name[:-3]):
            child_inst_name = c.name[:-3]
            child_inst = getattr(inst, child_inst_name)
            if child_inst:
                d[child_inst_name] = model2dict(child_inst, child_inst.__class__, exclude)

    return d


def model2json(inst, cls):
    d = model2dict(inst, cls)
    return json.dumps(d)


def load_json(inst, cls, json, exclude=None):
    parser = dict()
    parser['DATETIME'] = datetime_parser
    exclude = exclude if exclude else []

    for c in cls.__table__.columns:
        v = json.get(c.name)
        typestr = str(c.type)

        if c.name in exclude:
            continue
        elif not v:
            continue
        elif typestr in parser.keys():
            try:
                v = parser[typestr](v)
                setattr(inst, c.name, v)
            except:
                print "Error:  Failed to covert using ", str(
                    parser[typestr])
        else:
            setattr(inst, c.name, v)
    return inst