# coding: utf-8

from flask import Blueprint, jsonify, request, abort
from abceedapi import db, logger
from abceedapi.utils.auth_utils import cognito_auth_required
from .models import Advertisement

mod_ads = Blueprint('ads', __name__, url_prefix='/ads')


@mod_ads.route('/', methods=['GET'])
def list_ad():
    '''
    有効な広告一覧を取得する。最大５件
    '''
    #TODO handle for user doesn't login

    # print 'vao1'
    # remote_user = request.remote_user
    # if not remote_user or type(remote_user) is not dict:
    #     print 'test'
    #     # abort(401, {'message': 'unauthorized.'})
    #
    # try:
    #     cognito_pool_id = remote_user.get('cognitoIdentityPoolId')
    #     cognito_user_id = remote_user.get('cognitoIdentityId')
    # except:
    #     print 'test2'
    #     abort(401, {'message': 'unauthorized.'})


    ads = []
    if request.args.get('modify', False):
        ads = Advertisement.query.filter_by(is_expired=False).order_by(Advertisement.date_modified)
    else:
        ads = Advertisement.query.filter_by(is_expired=False).order_by(Advertisement.date_modified)[:5]
    print 'vao2'
    result = []
    for ad in ads:
        print 'vao3'
        result.append(ad.to_dict())
    print 'vao4'
    return jsonify(result), 200


@mod_ads.route('/', methods=['POST'])
def create_ad():
    if request.headers['Content-Type'] != 'application/json':
        abort(400, {'message': 'request must hame "Content-Type: applicatin/json" header.'})

    ad = Advertisement(
        image_url=request.json.get('image_url'),
        transition_url=request.json.get('transition_url'),
    )
    db.session.add(ad)
    db.session.commit()

    return jsonify(ad.to_dict()), 201


@mod_ads.route('/<ad_id>', methods=['PUT', 'DELETE'])
@cognito_auth_required(publisher=True)
def modify_ad(ad_id=None):
    ad = Advertisement.query.filter_by(id=ad_id).first()
    if not ad:
        abort(404, {'message': 'advertisement cannot be found.'})
    elif request.method == 'PUT':
        ad = ad.load_json(request.json)
    elif request.method == 'DELETE':
        db.session.delete(ad)
    db.session.commit()
    return jsonify(ad.to_dict())
    abort(500, {'message': 'internal server error.'})
