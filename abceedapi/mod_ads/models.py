# coding: utf-8

import uuid
from abceedapi import db
from abceedapi.models import Base
from abceedapi.utils.json_utils import model2json, model2dict


class Advertisement(Base):
    __tablename__ = 'advertisement'

    image_url = db.Column(db.Text, nullable=False)
    transition_url = db.Column(db.Text, nullable=False)
    is_expired = db.Column(db.Boolean, default=False, nullable=False)
    expired_date = db.Column(db.DateTime, nullable=True)
    views_count = db.Column(db.Integer, default=0, nullable=False)
    click_count = db.Column(db.Integer, default=0, nullable=False)

    # cognito_user_id
    # user = db.Column(db.String(255), nullable=True)

    def __init__(self, image_url, transition_url):
        self.image_url = image_url
        self.transition_url = transition_url
        super(Advertisement, self).__init__()

    def __repr__(self):
        return '<Advertisement %r>' % (self.id)

    @property
    def model2dict(self):
        return model2dict(self, self.__class__)
