# coding: utf-8

import logging
import json
from flask import Flask, jsonify, request, abort
from flask_sqlalchemy import SQLAlchemy
from flask_redis import FlaskRedis
from flask_cors import CORS, cross_origin
from sqlalchemy import func, create_engine

app = Flask(__name__)
app.config.from_object('abceedapi.config')
redis_store = FlaskRedis(app)

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

CORS(app)
db = SQLAlchemy(app)

db2 = create_engine('mysql://globee:Globee612@globee-report.c0pvfeqpfvfq.ap-northeast-1.rds.amazonaws.com:3306/abceed_report')
connection = db2.connect()

@app.route('/', methods=['GET'])
def ping(event=None, context=None, *args, **kwargs):
    print 'event = ', event
    print 'cookie = ', request.cookies
    print 'remote_user = ', request.remote_user
    return jsonify({'message': 'hello!'})


# @app.route('/layout', methods=['GET'])
# def layout():
#     return jsonify(ResultSet={
#         'system_tabs': [
#             {
#                 'name': 'BookDetailVCTab',
#                 'index': 0,
#                 'label': '学習',
#                 'icon_url': 'learnig.png'
#             }, {
#                 'name': 'AnalyticsVCTab',
#                 'index': 1,
#                 'label': '分析',
#                 'icon_url': 'analytics.png'
#             }, {
#                 'name': 'AccountVCTab',
#                 'index': 2,
#                 'label': 'アカウント',
#                 'icon_url': 'account.png'
#             }
#         ]
#     })


@app.route('/flushall', methods=['GET'])
def flushall():
    redis_store.flushall()
    return jsonify({'message': 'success'})


from .utils.auth_utils import cognito_auth_required
from .mod_books.models import Book
from .mod_users.models import PurchaseItem

@app.route('/purchase', methods=['POST'])
@cognito_auth_required()
def purchase(user=None):
    req_json = request.json if request.json else {}

    book_id = req_json.get('book_id')
    receipt_item_type = req_json.get('receipt_item_type')
    purchase_data = req_json.get('purchase_data')
    purchase_signature = req_json.get('purchase_signature')

    if not book_id:
        abort(400, {'message': 'book_id is requeired.'})
    if not receipt_item_type:
        abort(400, {'message': 'receipt_item_type is requeired.'})
    if not purchase_data:
        abort(400, {'message': 'purchase_data is requeired.'})
    if not purchase_signature:
        abort(400, {'message': 'purchase_signature is requeired.'})


    book = Book.query.filter_by(id=book_id).first()
    if book:
        purchase_item = PurchaseItem(
            book=book,
            user=user,
            receipt_item_type=receipt_item_type
        )
        purchase_item.load_json(req_json)
        db.session.add(purchase_item)
        db.session.commit()
        return jsonify(purchase_item.to_dict())
    abort(404, {'message': 'book cannot be found.'})


@app.route('/feedback', methods=['POST'])
def feedback():
    return jsonify({'message': 'success.'})
    abort(501, {'message': 'not implemented yet.'})


# Load modules
from abceedapi.mod_ads.controllers import mod_ads
from abceedapi.mod_users.controllers import mod_users
from abceedapi.mod_books.controllers import mod_books
from abceedapi.mod_analytics.controllers import mod_analytics

app.register_blueprint(mod_ads)
app.register_blueprint(mod_users)
app.register_blueprint(mod_books)
app.register_blueprint(mod_analytics)


@app.errorhandler(401)
@app.errorhandler(403)
@app.errorhandler(404)
@app.errorhandler(500)
@app.errorhandler(501)
def error_handler(error):

    result = {
        'error': {
            'code': error.code
        }
    }

    if type(error.description) is dict:
        result['error']['message'] = error.description.get('message')

    return jsonify(result), error.code
