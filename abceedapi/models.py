# coding: utf-8

import uuid
from abceedapi import db
from abceedapi.utils import json_utils

class Base(db.Model):
    __abstract__ = True

    id = db.Column(db.String(255), primary_key=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(),
                              onupdate=db.func.current_timestamp())

    def __init__(self):
        self.id = str(uuid.uuid4())

    def to_dict(self, exclude=None):
        return json_utils.model2dict(self, self.__class__, exclude=exclude)

    def load_json(self, json):
        return json_utils.load_json(self, self.__class__, json)
