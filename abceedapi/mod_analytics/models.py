# coding: utf-8

import uuid
import enum
from abceedapi import db
from abceedapi.models import Base


class AvgToeic(Base):
    __tablename__ = 'avg_toeic'

    total = db.Column(db.Float, default=0, nullable=False)

    listening = db.Column(db.Float, default=0, nullable=False)
    reading = db.Column(db.Float, default=0, nullable=False)

    part1 = db.Column(db.Float, default=0, nullable=False)
    part2 = db.Column(db.Float, default=0, nullable=False)
    part3 = db.Column(db.Float, default=0, nullable=False)
    part4 = db.Column(db.Float, default=0, nullable=False)
    part5 = db.Column(db.Float, default=0, nullable=False)
    part6 = db.Column(db.Float, default=0, nullable=False)
    part7 = db.Column(db.Float, default=0, nullable=False)

    def __init__(self):
        self.total = 0
        self.listening = 0
        self.reading = 0
        self.part1 = 0
        self.part2 = 0
        self.part3 = 0
        self.part4 = 0
        self.part5 = 0
        self.part6 = 0
        self.part7 = 0
        super(AvgToeic, self).__init__()


class TestStats(Base):
    __tablename__ = 'test_stats'

    candidates_count = db.Column(db.Float, nullable=True)
    total_score = db.Column(db.Float, nullable=True)
    avg_score = db.Column(db.Float, nullable=True)

    test_id = db.Column(db.String(255), db.ForeignKey('test.id'))
    test = db.relationship('Test', backref=db.backref('stats', lazy='dynamic'))

    def __repr__(self):
        return '<TestStats %r>' % (self.id)

    def __init__(self):
        self.candidates_count = 0
        self.total_score = 0
        self.avg_score = 0
        super(TestStats, self).__init__()

    def to_dict(self, exclude=None):
        test_stats_dict = super(TestStats, self).to_dict(exclude=exclude)

        if type(exclude) is list and 'test_stats_id' not in exclude:
            exclude.append('test_stats_id')
        else:
            exclude = ['test_stats_id']

        test_stats_dict['sections'] = []
        for section_stat in self.section_stats:
            section_stat_dict = section_stat.to_dict(exclude=exclude)
            test_stats_dict['sections'].append(section_stat_dict)

        return test_stats_dict

class SectionStats(Base):
    __tablename__ = 'test_section_stats'

    section_name = db.Column(db.String(255), nullable=False)
    total_score = db.Column(db.Float, nullable=True)
    avg_score = db.Column(db.Float, nullable=True)

    test_stats_id = db.Column(db.String(255), db.ForeignKey('test_stats.id'))
    test_stats = db.relationship('TestStats', backref=db.backref('section_stats', lazy='dynamic'))

    def __init__(self, test_stats, section_name):
        self.test_stats = test_stats
        self.section_name = section_name
        self.candidates_count = 0
        self.total_score = 0
        self.avg_score = 0
        super(SectionStats, self).__init__()

    def __repr__(self):
        return '<SectionStats %r>' % (self.id)

