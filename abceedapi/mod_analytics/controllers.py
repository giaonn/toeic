# coding: utf-8

from flask import Blueprint, jsonify, request, abort
from abceedapi import db, logger
from abceedapi.utils.auth_utils import cognito_auth_required
from .models import TestStats, SectionStats, AvgToeic
import copy

mod_analytics = Blueprint('analytics', __name__, url_prefix='/analytics')

@mod_analytics.route('/avg_toeic_test', methods=['GET'])
@cognito_auth_required()
def get_avg_test(user=None):
    user_id = user.id
    # user_id = '5cb00f62-87d4-46ce-a08a-81fe5dff3520'
    lst_ques          = get_all_question(user_id)
    lst_answer        = get_all_answer_right(user_id)
    lst_full_question = [6, 25, 39, 30, 30, 16, 54]
    if not lst_answer:
        return jsonify({"message" : "Cant get list answer"})

    if not lst_ques:
        return jsonify({"message" : "Cant get list question"})

    lstAvgPart = []

    leng_arr_ques   = len(lst_ques)
    leng_arr_answer = len(lst_answer)

    sum_answer_listening   = 0
    sum_question_listening = 0

    sum_answer_reading   = 0
    sum_question_reading = 0

    i = 0
    while(i<=6):
        #Get listen and read
        if i == 0 or i == 1 or i == 2 or i == 3:
            sum_answer_listening   += get_info_or_None(lst_answer[i], i, leng_arr_answer)
            sum_question_listening += get_info_or_None(lst_ques[i]  , i, leng_arr_ques)
        else:
            sum_answer_reading   += get_info_or_None(lst_answer[i], i, leng_arr_answer)
            sum_question_reading += get_info_or_None(lst_ques[i]  , i, leng_arr_ques)

        #Get x_part
        avg_score  = getX(get_info_or_None(lst_answer[i], i, leng_arr_answer),
                          get_info_or_None(lst_ques[i]  , i, leng_arr_ques),
                          lst_full_question[i])

        lstAvgPart.append(avg_score)
        i+=1

    avg_listening = getX(sum_answer_listening, sum_question_listening, 100)
    avg_reading   = getX(sum_answer_reading, sum_question_reading, 100)
    avg_total     = getX((sum_answer_listening + sum_answer_reading),(sum_question_listening + sum_question_reading), 200)

    data = get_result_avg_counting(lstAvgPart, avg_listening, avg_reading, avg_total)

    return jsonify(data)

def get_result_avg_counting(avg_part, avg_listening, avg_reading, avg_total):
    data = {
        'total_avg_score': avg_total,
        'total_full_score': 200,
        'sections': [
            {
                'name': 'Listening',
                'avg_score': avg_listening,
                'full_score': 100
            }, {
                'name': 'Reading',
                'avg_score': avg_reading,
                'full_score': 100
            }
        ],
        'parts': [
            {
                'name': 'Part 1',
                'avg_score': avg_part[0],
                'full_score': 6
            }, {
                'name': 'Part 2',
                'avg_score': avg_part[1],
                'full_score': 25
            }, {
                'name': 'Part 3',
                'avg_score': avg_part[2],
                'full_score': 39
            }, {
                'name': 'Part 4',
                'avg_score': avg_part[3],
                'full_score': 30
            }, {
                'name': 'Part 5',
                'avg_score': avg_part[4],
                'full_score': 30
            }, {
                'name': 'Part 6',
                'avg_score': avg_part[5],
                'full_score': 16
            }, {
                'name': 'Part 7',
                'avg_score': avg_part[6],
                'full_score': 54
            }
        ]
    }

    return data


def get_info_or_None(info, pos, len_arr):
    if pos < len_arr:
        return info
    return 0

def getX(a, b, full_score):
    if not a:
        a = 0
    if not b:
        b = 1
    b = b if  b!=0 else 1

    return round((a/float(b))*full_score, 1)

def get_all_question(user_id = None):
    user_id = "'" + user_id + "'"

    sql = ' select count(case question_type when 1 then 1 else null end)  as part1, ' \
          ' count(case question_type when 2 then 1 else null end)  as part2, ' \
          ' count(case question_type when 3 then 1 else null end)  as part3, ' \
          ' count(case question_type when 4 then 1 else null end)  as part4, ' \
          ' count(case question_type when 5 then 1 else null end)  as part5, ' \
          ' count(case question_type when 6 then 1 else null end)  as part6, ' \
          ' count(case question_type when 7 then 1 else null end)  as part7 ' \
          ' from question where test_id in ' \
          ' (select * from (select Q.test_id from question Q join user_activity_item U on Q.id = U.question_id ' \
          ' where U.user_id =  ' + user_id + ' '\
          ' and Q.question_type not in (8,9) ' \
          ' order by U.timestamp desc ' \
          ' limit 500) as T ' \
          ' group by T.test_id) '
    try:
        result = db.engine.execute(sql)
        arr = []
        for row in result:
            for item in row:
                arr.append(item)
        return arr
    except Exception, e:
        return None

def get_all_answer_right(user_id = None):
    user_id = "'" + user_id + "'"

    sql = ' select ' \
          ' coalesce(sum(case when T.learning_status in (3, 4, 5, 6, 7) and  T.question_type = 1 then T.score else 0 end),0) as part1, ' \
          ' coalesce(sum(case when T.learning_status in (3, 4, 5, 6, 7) and  T.question_type = 2 then T.score else 0 end),0) as part2, ' \
          ' coalesce(sum(case when T.learning_status in (3, 4, 5, 6, 7) and  T.question_type = 3 then T.score else 0 end),0) as part3, ' \
          ' coalesce(sum(case when T.learning_status in (3, 4, 5, 6, 7) and  T.question_type = 4 then T.score else 0 end),0) as part4, ' \
          ' coalesce(sum(case when T.learning_status in (3, 4, 5, 6, 7) and  T.question_type = 5 then T.score else 0 end),0) as part5, ' \
          ' coalesce(sum(case when T.learning_status in (3, 4, 5, 6, 7) and  T.question_type = 6 then T.score else 0 end),0) as part6, ' \
          ' coalesce(sum(case when T.learning_status in (3, 4, 5, 6, 7) and  T.question_type = 7 then T.score else 0 end),0) as part7 ' \
          ' from (select * from (select Q.question_type, U.learning_status, Q.score, Q.id from question Q ' \
          ' join user_activity_item U on Q.id = U.question_id where U.user_id = ' + user_id + ' and Q.question_type not in (8,9) ' \
          ' order by Q.id asc, U.timestamp desc limit 500) as T2 group by T2.id) as T '
    try:
        result = db.engine.execute(sql)
        arr = []
        for row in result:
            for item in row:
                arr.append(item)
        return arr
    except Exception, e:
        print e
        return None


@mod_analytics.route('/avg_toeic_test_old', methods=['GET'])
@cognito_auth_required()
def get_avg(user=None):
    # user_id = "'" + '5cb00f62-87d4-46ce-a08a-81fe5dff3520' + "'"
    user_id = "'" + user.id + "'"

    sql = ' select question_type, coalesce(sum(case when T.learning_status in (3, 4, 5, 6, 7) then T.score else 0 end),0) as score_right_answer, ' \
          ' from (select * from (select Q.question_type, U.learning_status, Q.score, Q.id from question Q ' \
          ' join user_activity_item U on Q.id = U.question_id where U.user_id = '+ user_id + ' and Q.question_type not in (8,9) ' \
          ' order by Q.id asc, U.timestamp desc limit 500) as T2 group by T2.id) as T ' \
          ' group by T.question_type ' \
          ' order by T.question_type asc '

    result = db.engine.execute(sql)
    avg_score  = 0
    full_score = 0

    avg_score_listening  = 0
    full_score_listening = 0

    avg_score_reading  = 0
    full_score_reading = 0
    part  = []
    dict  = {}

    for row in result:

        dict['name']       = 'Part ' + str(row[0])
        dict['avg_score']  = row[1]
        dict['full_score'] = row[2]
        part.append(copy.copy(dict))

        avg_score  += row[1]
        full_score += row[2]

        if int(row[0]) == 1 or int(row[0]) == 2 or int(row[0]) == 3 or int(row[0]) == 4:
            avg_score_listening  += row[1]
            full_score_listening += row[2]
        else:
            avg_score_reading  += row[1]
            full_score_reading += row[2]

    data = {
        'total_avg_score': avg_score,
        'total_full_score': full_score,
        'sections': [
            {
                'name': 'Listening',
                'avg_score' : avg_score_listening,
                'full_score': full_score_listening
            },{
                'name': 'Reading',
                'avg_score' : avg_score_reading,
                'full_score': full_score_reading
            }
        ],
        'parts': part
    }

    return jsonify(data)

@mod_analytics.route('/avg_toeic', methods=['GET'])
@cognito_auth_required()
def get_avg_toeic(user=None):
    avg = AvgToeic.query.order_by(-AvgToeic.date_modified).first()
    data = {
        'total_avg_score': avg.total,
        'total_full_score': 200,
        'sections': [
            {
                'name': 'Listening',
                'avg_score': avg.listening,
                'full_score':100
            },{
                'name': 'Reading',
                'avg_score': avg.reading,
                'full_score':100
            }
        ],
        'parts': [
            {
                'name': 'Part 1',
                'avg_score': avg.part1,
                'full_score': 6
            },{
                'name': 'Part 2',
                'avg_score': avg.part2,
                'full_score': 25
            },{
                'name': 'Part 3',
                'avg_score': avg.part3,
                'full_score': 39
            },{
                'name': 'Part 4',
                'avg_score': avg.part4,
                'full_score': 30
            },{
                'name': 'Part 5',
                'avg_score': avg.part5,
                'full_score': 30
            },{
                'name': 'Part 6',
                'avg_score': avg.part6,
                'full_score': 16
            },{
                'name': 'Part 7',
                'avg_score': avg.part7,
                'full_score': 54
            }
        ]
    }

    return jsonify(data)
