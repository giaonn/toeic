# Abceed API with AWS Lambda

Zappa + Flaskを利用し、AWS APIGateway + AWS Lambdaで動作します。
また、ユーザ認証・認可、ファイル管理、スケジューラ、ログ管理において各種AWSサービスを利用するため、
それぞれの動作部分はAWSの各種ドキュメントを参照してください。

* https://github.com/Miserlou/Zappa
* http://flask.pocoo.org/

* Cognito
* S3
 * abceed.source.tokyo
 * cdn.abceed2.com
* RDS
 * abceed-db (abceed-db.c0pvfeqpfvfq.ap-northeast-1.rds.amazonaws.com:3306)
 * ローカル環境でのテスト実行のため、SecurityGroupにてローカル環境からリモート接続可能にすることを推奨
* SNS
* CloudWatch

## Install & Setup

Linux環境を用意し、以下のpython環境を準備してください

* python 2.7.x
* pip
* pyenv
* virtualenv

> 簡単のためVagrantfile[./docs/Vagrantfile]およびpython環境自動設定用のスクリプト[./docs/setup.sh]を提供します

AWS Credentials を設定する

```bash
$ pip install awscli
$ aws configure
AWS Access Key ID [None]: ********************
AWS Secret Access Key [None]: ****************************************
Default region name [None]: ap-northeast-1
Default output format [None]: json

$ cat ~/.aws/config
[default]
output = json
region = ap-northeast-1

$cat ~/.aws/credentials
[default]
aws_access_key_id = ********************
aws_secret_access_key = ****************************************
```

virtualenv環境の作成

```
$ cd ~/
$ virtualenv env
$ source env/bin/activate
```

python依存パッケージのインストール

```bash
$ yum install -y gcc libffi-devel python-devel openssl-devel zlib-devel bzip2 \
    bzip2-devel readline-devel sqlite sqlite-devel openssl-devel git \
$ git clone https://thisrepository.com/repository.git
$ cd ./abceedapi
$ pip isntall -r requirements.txt
```

zappa_flameworkへ独自パッチ(zappa_with_iamauth.patch)を適用する

```bash
$ cd path/to/patch
$ patch -u ~/env/lib/python2.7/site-packages/zappa/wsgi.py < zappa_with_iamauth.patch
```

アプリケーションを起動する。

```bash
$ export SQLALCHEMY_DATABASE_URI='mysql://globee:******@abceed-db.c0pvfeqpfvfq.ap-northeast-1.rds.amazonaws.com:3306/abceed_db'
$ python run.py
```

アプリケーションのデプロイ(本番環境が更新されるので注意)。

```
$ zappa deploy dev
$ zappa schedule dev
```

## 運用

serverログ参照

```
$ zappa tail dev
```

初回DBテーブル作成

```bash
$ export SQLALCHEMY_DATABASE_URI='mysql://globee:******@abceed-db.c0pvfeqpfvfq.ap-northeast-1.rds.amazonaws.com:3306/abceed_db'
$ python init_db.py
```

> If the environment variable (SQLALCHEMY_DATABASE_URI) is not set, the sqlite file is created locally

## Structure

```
├── abceedapi
│   ├── __init__.py 	// app route
│   ├── config.py 		// settings
│   ├── models.py 		// base model for O/R mapper (sqlalchemy)
│   ├── mod_ads
│   │   ├── controllers.py 		// map to /ads url
│   │   └── models.py 			// models for advertisement
│   ├── mod_analytics
│   │   ├── controllers.py
│   │   └── models.py
│   ├── mod_books
│   │   ├── controllers
│   │   │   ├── author_controllers.py
│   │   │   ├── book_controllers.py
│   │   │   ├── category_controllers.py
│   │   │   ├── publisher_controllers.py
│   │   │   ├── question_controllers.py
│   │   │   └── test_controllers.py
│   │   └── models
│   │       ├── book_model.py
│   │       ├── question_model.py
│   │       └── test_model.py
│   ├── mod_users
│   │   ├── controllers.py
│   │   └── models.py
│   ├── scheduler
│   │   └── tasks.py 				// Batch processing
│   └── utils
│       ├── auth_utils.py
│       └── json_utils.py
├── data 							// Book data import script
├── docs
├── init_db.py
├── libmysqlclient.so.18
├── MySQLdb
├── _mysql_exceptions.py
├── _mysql.so
├── README.md
├── requirements.txt
├── run.py 							// Local startup script
├── zappa_settings.json 			// zappa settings
└── zappa_with_iamauth.patch 		// zappa patch (Required Adaptation!!)

```
